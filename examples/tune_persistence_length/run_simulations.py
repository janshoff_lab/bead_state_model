from typing import Dict, Any, Tuple
from multiprocessing import Pool
import os
import sys
import time
import json
import numpy as np
from bead_state_model.system_setup import (system_setup,
                                           simulation_setup,
                                           add_filaments_from_file,
                                           add_filaments)
from bead_state_model.data_reader import DataReader


kernel = 'SingleCPU'
progress_output_stride = int(1e4)


def _mute():
    sys.stdout = open(os.devnull, 'w')

def run_sim(idx: int) -> Tuple[float, float]:
    """
    Run one simulation with given parameters. Simulation is split into parts,
    to allow viewing interim results of the simulation and make it possible 
    to continue simulation from a check point.

    :param params: Dictionary defining parameters for simulation.
    :return: Run time for simulation measured with time.time and time.process_time.
    """

    file_params = 'initial_states/filament{:04}/params.json'.format(idx)

    with open(file_params, 'rt') as fh:
        params = json.load(fh)

    params['out_dir'] = 'simulations/out{:04}'.format(idx)
    params['index'] = idx

    print('running simulation {:04}'.format(idx))
    t_start = time.time()
    pt_start = time.process_time()

    # determine number of steps per part 
    n_steps = params['n_steps']
    obs_interval = params['observation_interval']
    n_steps_interval = obs_interval * params['next_part_after_n_frames']

    if n_steps % n_steps_interval != 0:
        msg = "can run simulations only if n_steps is a multiple of n_steps_interval!"
        raise RuntimeError(msg)
    
    for i in range(n_steps // n_steps_interval):
        params_i = params.copy()
        params_i['n_steps'] = n_steps_interval
        run_sim_part(i, params_i)

    t_end = time.time()
    pt_end = time.process_time()

    t = t_end - t_start
    pt = pt_end - pt_start

    print('done with simulation {}'.format(idx))
    
    return t, pt


def run_sim_part(i: int, params: Dict[str, Any]):
    """
    Run one part of the simulation. If part index ``i`` is 0, the simulation part
    is initialized from a filament in folder ``initial_states``. 
    Otherwise it's initialized from final state of previous part.
    """
    sim_idx = params['index']
    n_steps = params['n_steps']
    obs_interval = params['observation_interval'] 
        
    folder_output = params['out_dir']
    folder_part_template = os.path.join(folder_output, 'part_{:04}')
    folder_part = folder_part_template.format(i)
    os.makedirs(folder_part, exist_ok=True)

    folder_filament = 'initial_states/filament{:04}'.format(sim_idx)

    box = np.array([params['box_size']['x'],
                    params['box_size']['y'],
                    params['box_size']['z']])
    k_bend = params['k_bend']/2
    k_stretch = params['k_stretch']/2
    dt = params['time_step']
    
    system, filament_handler = system_setup(box_size=box,
                                            k_bend=k_bend,
                                            k_stretch=k_stretch,
                                            n_beads_max=int(2e2))
    
    simulation = simulation_setup(system, filament_handler, kernel,
                                  force_monitor_threshold=0.5/dt)

    if i == 0:
        file_beads = os.path.join(folder_filament, 'beads.txt')
        beads, filaments, links = add_filaments_from_file(simulation,
                                                          file_beads,
                                                          offset=-box/2)
        map_fil_idx = np.arange(len(filaments))
        map_bead_idx = np.arange(len(beads))
    else:
        folder_previous_part = folder_part_template.format(i-1)
        file_data = os.path.join(folder_previous_part, 'data.h5')
        file_links = os.path.join(folder_previous_part, 'links.h5')
        dr = DataReader(file_data, file_links)
        positions = np.load(os.path.join(folder_previous_part, 'positions_final_frame.npy'))
        n_frames = dr.links_h5.attrs['n_datasets']
        filaments, links = dr.get_filaments(n_frames-1)
        map_fil_idx, map_bead_idx, filaments_new, links_new =\
            add_filaments(simulation, positions, filaments, links)
    
    with open(os.path.join(folder_part, 'parameters.json'), 'wt') as fh:
        json.dump(params, fh)

    n_steps = params['n_steps']
    
    filament_handler.initialize(filaments, links)
    
    file_output = os.path.join(folder_part, 'data.h5')
    simulation.output_file = file_output
    simulation.record_trajectory(stride=obs_interval)
    simulation.observe.particles(obs_interval,
                                 callback=filament_handler.write,
                                 save=False)
    simulation.progress_output_stride = progress_output_stride
    simulation.skin = 0.5

    filament_handler.output_file = os.path.join(folder_part, 'links.h5')
    if os.path.exists(filament_handler.output_file):
        os.remove(filament_handler.output_file)

    simulation.run(n_steps, dt)

    positions_final = np.full((len(simulation.current_particles), 3), np.nan)
    for i, p in enumerate(simulation.current_particles):
        positions_final[i] = p.pos

    np.save(os.path.join(folder_part, 'positions_final_frame.npy'), positions_final)
    np.save(os.path.join(folder_part, 'map_filaments_previous_part.npy'), map_fil_idx)
    np.save(os.path.join(folder_part, 'map_beads_previous_part.npy'), map_bead_idx)


if __name__ == "__main__":

    if not os.path.exists('simulations'):
        os.mkdir('simulations')
    sim_indices = list(range(120))
    n_proc = 4
    pool = Pool(n_proc, initializer=_mute)
    pool.map(run_sim, sim_indices)
