from typing import Iterable
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import binned_statistic
from scipy.optimize import curve_fit
from bead_state_model.data_reader import DataReader
from bead_state_model.periodic_boundaries import get_minimum_image_vector


def get_cos_theta(folders: Iterable[str], times: Iterable[int], parts: int=2) -> np.ndarray:
    """
    Get cos(theta) from filaments in list folders (each folder
    has to contain a ReaDDy trajectory file of a simulation of a single filament).
    Retrieved data will be merged into one array with two columns, first
    column are distances along the filament, second column are cos(theta) values.
    """

    data_all = []
    for f in folders:
        data_f = []
        dr = DataReader(f+'/part_{:04}/data.h5', n_parts=parts)
        box = dr.read_box_size()
        box = np.array([box[1, 0] - box[0, 0],
                        box[1, 1] - box[0, 1],
                        box[1, 2] - box[0, 2]])
        positions = dr.read_particle_positions()
        for t in times:
            pos_t = positions[t] + box/2
            segment_vectors = get_minimum_image_vector(pos_t[1:], pos_t[:-1],  box)
            segment_lengths = np.sqrt((segment_vectors**2).sum(1))
            contour = segment_lengths.cumsum()
            contour_shift = np.zeros_like(contour)
            contour_shift[1:] = contour[:-1]
            data_f_t = []
            for d in range(1, len(pos_t)-1):
                data_f_t_d = np.full((len(pos_t)-d-1, 2), np.nan)
                cos_theta = ((segment_vectors[d:] * segment_vectors[:-d]).sum(1)
                             / segment_lengths[d:] / segment_lengths[:-d])
                lengths = (segment_lengths[d:] + segment_lengths[:-d])*0.5
                if d != 1:
                    lengths += contour[d-1: -1] - contour_shift[1: -d+1]
                data_f_t_d[:, 0] = lengths
                data_f_t_d[:, 1] = cos_theta
                data_f_t.append(data_f_t_d)
            data_f.append(np.concatenate(data_f_t))
        data_all.append(np.concatenate(data_f))
    return np.concatenate(data_all)


def exp_(l, lp):
    return np.exp(-l/lp)


if __name__ == "__main__":

    plt.rcParams.update({
        "text.usetex": True,
        "lines.linewidth": 2.5,
        "font.size": 17
    })

    n_bins = 17
    
    index = pd.read_csv('index.csv', index_col=0)

    results_dir = os.path.join('results', 'cos_theta')
    os.makedirs(results_dir, exist_ok=True)

    folder_sim_template = os.path.join('simulations', 'out{:04}')

    fig, ax = plt.subplots(1, 1)
    fig_fit, ax_fit = plt.subplots(1, 1)

    set_k_bend = index['k_bend'].unique()
    fit_results = pd.DataFrame(np.full(len(set_k_bend), np.nan),
                               index=pd.Series(set_k_bend, name='k_bend'),
                               columns=['l_p'])

    for i, k_bend in enumerate(set_k_bend):
        selected = index[index['k_bend'] == k_bend]
        folders = [
            folder_sim_template.format(idx) for idx in selected.index
        ]
        frames = [0, 2000, 4000]

        cos_theta = get_cos_theta(folders, frames)

        mean, edges, numbers = binned_statistic(cos_theta[:, 0],
                                                cos_theta[:, 1],
                                                bins=n_bins)

        ax.plot((edges[1:] + edges[:-1])/2, mean,
                label=f'$k_\\mathrm{{bend}}={k_bend:.1f}$')
        ax_fit.plot((edges[1:] + edges[:-1])/2, mean,
                    label=f'$k_\\mathrm{{bend}}={k_bend:.1f}$')

        try:
            x_exp = (edges[1:] + edges[:-1])/2
            w = x_exp < 20
            opt = curve_fit(exp_, x_exp[w], mean[w])[0]
            print(f"L_p(k_b={k_bend}) = {opt[0]}")        
            ax_fit.plot(x_exp[w], exp_(x_exp[w], *opt), f'C{i}--')
            fit_results.loc[k_bend, 'l_p'] = opt[0]
        except ValueError as e:
            print("Error during fit: ", e)

    for axi in [ax, ax_fit]:
        axi.legend()
        axi.set(
            xlabel='$\\Delta s/$bead diameter',
            ylabel='$\\langle \\cos\\theta \\rangle$',
            xlim=[-0.5, 35.5],
        )

    figname = os.path.join(results_dir, 'cos_theta_bins_{:03}'.format(n_bins))
    fig.savefig(figname + '.svg')
    figname = os.path.join(results_dir, 'cos_theta_bins_{:03}_fit'.format(n_bins))
    fig_fit.savefig(figname + '.svg')
    fit_results.to_csv(os.path.join(results_dir, 'fit_results.csv'))
    plt.show()
