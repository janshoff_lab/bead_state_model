from typing import Dict, Union, Iterable, Any
import os
from multiprocessing import Pool
import time
import json
import numpy as np
import pandas as pd
from bead_state_model.network_assembly.create_filament import (create_filament,
                                                               write_beads_file,
                                                               write_filaments_file)


ParamSet = Iterable[Union[int, float, str]]

def create_single_filament(params: Dict[str, Any]):
    """
    This function does the actual work of creating a filament and
    writing the coordinates to the correct folder.
    """
    idx = params.pop('index')
    folder = 'initial_states/filament{:04}'.format(idx)
    file_config = os.path.join(folder, 'params.json')
    file_beads = os.path.join(folder, 'beads.txt')
    file_filaments = os.path.join(folder, 'filaments.txt')    
    n_beads = params['n_beads']
    k_stretch = params['k_stretch']
    k_bend = params['k_bend']
    box = np.array([params['box_size']['x'],
                    params['box_size']['y'],
                    params['box_size']['z']])
    
    print("creating", folder)
    t = str(int(time.time()*1e6))
    h = hash(folder + t)
    s = abs(int(str(h)[-9:]))
    np.random.seed(s)

    os.makedirs(folder, exist_ok=True)    
    with open(file_config, 'wt') as fh:
        json.dump(params, fh)
        
    coords, links = create_filament(n_beads, box, k_stretch, k_bend)
    write_beads_file(file_beads, n_beads, coords, links)
    write_filaments_file(file_filaments, n_beads)

def create_param_sets(params_dict: Dict[str, ParamSet],
                      multiplier: int, idx_offset: int=0) -> pd.DataFrame:
    """
    Create all permutations of varied parameters, insert them in to 
    a pandas DataFrame with named columns.
    """
    labels = []
    params = []
    for lbl in sorted(params_dict.keys()):
        labels.append(lbl)
        params.append(params_dict[lbl])

    n_param_sets = multiplier
    for p in params:
        n_param_sets = n_param_sets * len(p)        

    param_sets = pd.DataFrame(columns=labels)
    
    p_set = [[p[0] for p in params]]
    filled_sets = pd.DataFrame(p_set * multiplier, columns=labels)
    param_sets = param_sets.append(filled_sets)
    for j, p in enumerate(params):
        lbl = labels[j]
        for k, pp in enumerate(p[1:]):
            param_sets_k = filled_sets.copy()
            param_sets_k[lbl] = pp
            param_sets = param_sets.append(param_sets_k)
        filled_sets = param_sets.copy()

    param_sets.index = np.arange(len(param_sets)) + idx_offset
    return param_sets

    
if __name__ == "__main__":

    # Prepare multiprocessing. You can increase this to match the number of your available threads,
    # but creating single filaments is quite fast. (This is more in preparation for creating whole
    # networks instead of single filaments, where running things in parallel saves a lot of time.)
    n_proc = 2
    pool = Pool(n_proc)

    varied_params = {'k_bend': [13, 26]}

    # if you later create more initial states, you can use offset to shift
    # the indices, to prevent overwriting previoulsy generated initial states / results.
    offset = 0
    param_sets = create_param_sets(varied_params, 60, offset)

    folder = 'initial_states'
    if not os.path.exists(folder):
        os.mkdir(folder)
    
    fname = 'index.csv'
    if os.path.exists(fname):
        # This is only relevant if you have run this script before and want to create
        # more initial states without deleting the old ones. If you are fine with
        # deleting old ones, simply keep offset=0 and delete the folder ``initial_states``.
        old_param_sets = pd.read_csv(fname, index_col=0)
        combined_param_sets = pd.concat([old_param_sets, param_sets])
        if len(combined_param_sets.index.unique()) != len(combined_param_sets.index):
            msg = "tried to concatenate old and new index tables, but indices "
            msg += "some indices appear in both tables"
            raise RuntimeError(msg)
        combined_param_sets.to_csv(fname)
    else:
        param_sets.to_csv(fname)

    with open('default_params.json', 'rt') as fh:
        default_config = json.load(fh)

    param_list = []
    for i in range(offset, offset+len(param_sets)):
        params = default_config.copy()
        d = param_sets.loc[i].to_dict()
        d['index'] = i
        params.update(d)
        param_list.append(params)        
    
    pool.map(create_single_filament, param_list)
