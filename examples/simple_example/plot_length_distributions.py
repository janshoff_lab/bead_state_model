from typing import Union, List, Tuple
import argparse

import matplotlib.pyplot as plt
import numpy as np

from bead_state_model.data_reader import DataReader
from actomyosin_analyser.analysis.analyser import Analyser
from actomyosin_analyser.analysis import geometry
from scipy.stats import gaussian_kde


def main(frames: List[int],
         style: Union[str, None]):
    if style is not None:
        plt.style.use(style)

    fig, ax = plt.subplots(1, 1)

    dr = DataReader('simulation_output/data.h5')
    analyser = Analyser(dr, 'analysis.h5')

    _plot_length_distributions(ax, analyser, frames)

    fig.tight_layout()
    fig.savefig('distributions.svg')


def _plot_length_distributions(
        ax: plt.Axes,
        analyser: Analyser,
        frames: List[int]
):
    kdes = []
    global_min = 1e5
    global_max = 0.0
    for f in frames:
        kde, min_, max_ = _get_lengths_kde_single_frame(
            analyser, f)
        global_min = min(global_min, min_)
        global_max = max(global_max, max_)
        kdes.append(kde)

    x = np.linspace(global_min, global_max, 200)

    for i, f in enumerate(frames):
        kde = kdes[i]
        ax.plot(x, kde(x), label=f'frame {f:02}')

    ax.legend()
    ax.set(
        xlabel='$l / x_0$',
        ylabel='density',
        title='Length distributions'
    )


def _get_lengths_kde_single_frame(
        analyser: Analyser,
        frame: int
) -> Tuple[gaussian_kde, float, float]:
    coords = analyser.get_trajectories_filaments()[frame]
    filaments = analyser.get_filaments()[frame]

    box = analyser.data_reader.read_box_size()
    box = np.array([box[1, 0] - box[0, 0],
                    box[1, 1] - box[0, 1],
                    box[1, 2] - box[0, 2]])

    lengths = []
    
    for f in filaments:
        fcoords = coords[f.items]
        v_segments = geometry.get_minimum_image_vector(
            fcoords[:-1], fcoords[1:], box)
        d = np.sqrt(np.sum(v_segments**2, 1))
        lengths.append(d)

    lengths = np.concatenate(lengths)

    kde = gaussian_kde(lengths)

    return kde, lengths.min(), lengths.max()
    
        
if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('frames', type=int, nargs='+')
    parser.add_argument('--style', '-s')
    args = parser.parse_args()

    main(args.frames, args.style)
