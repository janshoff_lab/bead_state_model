from bead_state_model.data_reader import DataReader
from actomyosin_analyser.analysis.analyser import Analyser
from actomyosin_analyser.file_io.xyz_exporter import XYZExporter

dr = DataReader('simulation_output/data.h5')
a = Analyser(dr, 'simulation_output/analysis.h5')

expo = XYZExporter()

expo.export_all(a, folder='simulation_output/xyz')
