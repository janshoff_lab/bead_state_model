import matplotlib.pyplot as plt
import numpy as np

from bead_state_model.data_reader import DataReader
from actomyosin_analyser.analysis.analyser import Analyser

dr = DataReader('simulation_output/data.h5')
analyser = Analyser(dr, 'simulation_output/analysis.h5')
coordinates = analyser.get_trajectories_filaments()
filaments = analyser.get_filaments()

# select 10 indices
selected_indices = np.random.choice(
    np.arange(len(filaments[0])),
    10
)

for idx in selected_indices:
    c_idx = coordinates[0, filaments[0][idx].items]
    x = c_idx[:, 0]
    y = c_idx[:, 1]
    plt.plot(x, y, '-o')

plt.xlabel('$x/x_0$')
plt.ylabel('$y/x_0$')

plt.gca().set(
    aspect='equal'
)

plt.show()
