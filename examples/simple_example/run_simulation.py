import os
from bead_state_model import Simulation, SimulationParameters


def main():
    """
    This function gets executed when you run the script
    with `python run_simulation.py`. The call
    of the function happens at the very end of this script.
    """

    if not os.path.exists('initial_state'):
        print("No folder initial_state found. Run the script "
              "create_network.py before run_simulation.py")
        return

    box, k_stretch, k_bend = SimulationParameters.load_parameters_from_generated_network(
        'initial_state/config.json'
    )

    parameters = SimulationParameters(
        box_size=box,
        k_bend=k_bend,
        k_stretch=k_stretch,
        n_beads_max=5000,
        rate_motor_bind=0.1,  # set this to 0 if you want to disable links
        rate_motor_step=0.0,  # assign a positive value here if you want
                              # your links to behave like active motors
                              # instead of passive cross-links
        n_max_motors=500  # this limits the maximum number of
                          # motors/links in a simulation
    )

    s = Simulation(output_folder='simulation_output',
                   parameters=parameters)

    # Add filaments/beads from file initial_state/beads.txt .
    s.add_filaments('initial_state/beads.txt')

    observation_interval = 100 # record one frame every 100 time steps
    # You need to enable record_trajectory if you want to use
    # bead_state_model's DataReader.
    # now we are all set to run the simulation
    n_steps = 2000
    dt = 0.006
    s.run(n_steps, dt, observation_interval)


if __name__ == "__main__":
    """
    This if statement makes sure that the following parts are
    only executed if the script is directly executed, that
    usually happens when you run `python run_simulation.py`
    in a terminal.
    This means that you can still safely import functions
    from this file without executing anything, if you
    want to build your own version of a simulation run 
    script.
    """
    main()
