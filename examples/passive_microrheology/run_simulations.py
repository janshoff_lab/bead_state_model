import argparse
import json
import os
import time
from multiprocessing import Pool
from typing import Dict, Sequence, List, Any

import numpy as np
import pandas as pd

from bead_state_model import Simulation

# Parameters to be varied in the simulations. The default
# has 4 values for rate_motor_bind, and 6 different initial_states,
# leading to 4*6 = 24 simulations.
from bead_state_model.data_reader import DataReader

from run_equilibrations import MRBeadSetupHandler

varied_parameters = {
    'rate_motor_bind': [0.0, 0.1, 0.25, 0.5],
    'initial_state_index': np.arange(6)
}

# The index offset. Adjust this, if you already ran this script, and
# later want to run some more (e.g. a repetition with the same parameters,
# because you need to aggregate more data, or a set of different variations
# of rate_motor_bind/initial_state_index.
offset = 0

n_repetitions = 2

ParamSet = Dict[str, Any]


def main(n_processes: int):
    """
    This function gets executed when you run the script
    with `python run_simulation.py`. The call
    of the function happens at the very end of this script.
    """

    parameter_table = _create_parameter_table(varied_parameters, 2, offset)

    if not os.path.exists('simulations'):
        os.mkdir('simulations')
    fname_sim_index = 'simulations/simulation_index.csv'
    _write_parameter_table(parameter_table, fname_sim_index)
    parameter_sets = _generate_parameter_sets(parameter_table)

    pool = Pool(n_processes)
    results = pool.map(run_simulation, parameter_sets)

    r = np.array(results)
    # store some information on run time of each simulation
    np.savetxt('duration_of_simulations.txt', r, header='# run_time')


def run_simulation(parameters: ParamSet) -> float:
    # copy and retrieve via pop to trim the parameters
    # down to valid parameters for the Simulation class
    parameters = parameters.copy()
    idx = parameters.pop('index')
    print('running simulation {:03}'.format(idx))
    t_start = time.time()

    output_folder = parameters.pop('out_dir')
    dt = parameters.pop('dt')
    n_steps = parameters.pop('n_steps')
    obs_interval = parameters.pop('observation_interval')
    network_index = parameters.pop('initial_state_index')
    mr_bead_parameters = parameters.pop('microrheology_bead')
    # get diffusion constant by calculating the radius relative
    # to polymeric bead radius (set to 0.5)
    mr_bead_diffusion_const = 0.5/mr_bead_parameters['radius']

    mr_bead_setup_handler = MRBeadSetupHandler(radius=mr_bead_parameters['radius'],
                                               k_repulsion=mr_bead_parameters['k_repulsion'])

    s = Simulation(
        output_folder=output_folder,
        parameters=parameters,
        non_filament_particles={'bead': mr_bead_diffusion_const},
        interaction_setup_handler=mr_bead_setup_handler
    )

    init_state_file = os.path.join(
        'initial_states_equilibrated',
        'network{:03}'.format(network_index),
        'data.h5'
    )

    mr_bead_coordinates = _get_mr_bead_coordinates(init_state_file)
    s.add_non_filament_particles(bead=mr_bead_coordinates)

    s.add_filaments(init_state_file)

    s.run(n_steps, dt, obs_interval, mute=True)

    t_end = time.time()
    t = t_end - t_start

    print('done with simulation {:03}'.format(idx))

    return t


def _get_mr_bead_coordinates(init_state_file) -> np.ndarray:
    dr = DataReader(init_state_file)
    positions_final_frame = dr.read_particle_positions(minimum_image=True)[-1]
    # there is only a single mr bead, it's the first particle in the array:
    coords = positions_final_frame[0]
    return coords[np.newaxis, :]


def _create_parameter_table(
        params_dict: Dict[str, Sequence[float]],
        multiplier: int, idx_offset: int = 0
) -> pd.DataFrame:
    """
    Create parameter sets. Iterate over all combinations defined in ``param_dict``,
    and store combinations in a DataFrame.

    :param params_dict: Defines which values should be varied for the simulations, all
                        combinations will be created and stored in a DataFrame.
    :param multiplier: Defines number of repetitions for each unique parameter combination.
    :param idx_offset: Offset of indices for DataFrame (use this if your index already
                       contains parameters from previous runs).

    :return: DataFrame with sequential integer index (starting at idx_offset, ending at
             idx_offset + n_combinations * multiplier), and one column per
             varied parameter.
    """
    labels = []
    params = []
    for lbl in sorted(params_dict.keys()):
        labels.append(lbl)
        params.append(params_dict[lbl])

    n_param_sets = multiplier
    for p in params:
        n_param_sets = n_param_sets * len(p)

    table = pd.DataFrame(columns=labels)

    p_set = [[p[0] for p in params]]
    filled_sets = pd.DataFrame(p_set * multiplier, columns=labels)
    table = table.append(filled_sets)
    for j, p in enumerate(params):
        lbl = labels[j]
        for k, pp in enumerate(p[1:]):
            param_sets_k = filled_sets.copy()
            param_sets_k[lbl] = pp
            table = table.append(param_sets_k)
        filled_sets = table.copy()

    table.index = np.arange(len(table)) + idx_offset
    return table


def _write_parameter_table(table: pd.DataFrame, fname: str):
    if os.path.exists(fname):
        # Add parameter sets to existing index e.g. from previous run
        # of this script. Adjust offset accordingly! Index needs to have
        # the exact same columns defined, i.e. the varied parameters have
        # to identical as in any previous runs.
        old_param_sets = pd.read_csv(fname, index_col=0)
        combined_param_sets = pd.concat([old_param_sets, table])
        if len(combined_param_sets.index.unique()) != len(combined_param_sets.index):
            msg = "tried to concatenate old and new index tables, but indices "
            msg += "some indices appear in both tables"
            raise RuntimeError(msg)
        combined_param_sets.to_csv(fname)
        return
    # Save index.
    table.to_csv(fname)


def _generate_parameter_sets(table: pd.DataFrame) -> List[ParamSet]:
    with open('default_params.json', 'rt') as fh:
        default_config = json.load(fh)
    param_list = []
    # for i in range(offset, offset+len(param_sets)):
    for i in table.index:
        # load default parameters
        params = default_config.copy()
        # get current parameter set as dictionary
        d = table.loc[i].to_dict()
        # add entry required for ``run_sim`` function
        d['index'] = i
        params['out_dir'] = os.path.join('simulations', 'out{:04}'.format(i))
        # update copy of default params with varied params
        params.update(d)
        param_list.append(params)
    return param_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('number_of_processes', type=int)
    args = parser.parse_args()

    main(args.number_of_processes)
