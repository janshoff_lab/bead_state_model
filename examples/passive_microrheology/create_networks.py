"""
Creates a few networks and writes all coordinates and links (forming
filaments) into folder `initial_states`.
After this, you should run the script `run_equilibration.py`, and 
finally `run_simulations.py`.

2020-06-17 -- Ilyas Kuhlemann
"""

from typing import Dict, Any
import argparse
import os
import time
import json
from multiprocessing.pool import Pool

import numpy as np
import pandas as pd

from bead_state_model.network_assembly.create_network import AcceptanceRateHandlerBeadInNetwork
from bead_state_model.network_assembly.create_network_simple import CreateNetworkSimple

# read some parameter from default config
with open('default_params.json', 'rt') as fp:
    config = json.load(fp)

# size of simulation BOX
# (with periodic boundary conditions)
box = np.array(config['box_size'])
# Parameters for filament stiffness
k_bend = config['k_bend']
k_stretch = config['k_stretch']

radius_bead = config['microrheology_bead']['radius']  # default = 7.41 = 4 microns / 0.54 microns
                                                      # (see documentation on tuning the length scale)
k_repulsion = config['microrheology_bead']['k_repulsion']


def create_network(folder: str) -> Dict[str, Any]:
    """
    Create a network with global parameters defined in this file.
    Write final state to specified folder.

    :return: Dictionary with information on created network, e.g. number of filaments,
             beads per filament, etc.
    """
    np.random.seed(np.uint32(hash(folder + str(time.time()))))
    
    system = CreateNetworkSimple(
        box=box,
        k_bend=k_bend,
        k_stretch=k_stretch,
        n_filaments=700,
        n_beads_per_filament=25
    )

    arh = AcceptanceRateHandlerBeadInNetwork(n_batch=30,
                                             radius=radius_bead,
                                             k_repulsion=k_repulsion,
                                             position=box/2)
    system.acceptance_rate_handler = arh

    system.run(verbose=False)
    system.write(folder)
    print('created \'{}\''.format(folder))
    return system.get_info()


def load_index() -> pd.DataFrame:
    """
    Load index if it exists, prepare an empty data frame if it does not.
    """
    fname = 'initial_states/index.csv'
    if os.path.exists(fname):
        return pd.read_csv(fname)
    return pd.DataFrame(columns=['index',
                                 'n_filaments',
                                 'n_beads_max',
                                 'n_beads_mean',
                                 'n_beads_std',
                                 'n_beads_total'])


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'number_of_processes', type=int,
        help=("Number of processes to run in parallel, "
              "adjust this to match the number of threads you want to use.")
    )
    args = parser.parse_args()

    pool = Pool(args.number_of_processes)

    # set up sequentially numbered folders for generated networks
    indices = list(range(6))
    folders = ['initial_states/network{:03}'.format(i) for i in indices]
    os.makedirs('initial_states', exist_ok=True)

    # run create network algorithm with n_proc processes in parallel
    results = pool.map(create_network, folders)

    network_index = load_index()
    # add created networks to index
    for idx, res in zip(indices, results):
        network_index.loc[len(network_index)] = [
            idx,
            res['n_filaments'],
            res['n_beads_max'],
            res['n_beads_mean'],
            res['n_beads_std'],
            res['n_beads_total']
        ]
    network_index.to_csv('initial_states/index.csv', index=None)
