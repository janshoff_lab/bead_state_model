import readdy

sim_index = 0

# The ".." means to go one directory up. So this script assumes it is one level below the folder
# where the run_simulations.py lies. Otherwise you have to change the path.
file_data = f"../simulations/out{sim_index:04}/data.h5"
traj = readdy.Trajectory(file_data)
traj.convert_to_xyz(generate_tcl=False)
