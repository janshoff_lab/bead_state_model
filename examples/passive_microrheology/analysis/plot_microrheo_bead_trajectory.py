""" This script shows how to get trajectories of non-filament particles from simulation data.

Reading the data is shown via the Analyser class, which is part of a different project.
It's not necessary to use it, but it can speed things up a lot. When you run this for
the first time you won't see any speed up. But when you run this script again it
will be way faster, especially when you recorded more than just the 20 frames
that are used in this example by default.

Analyser loads the raw data from the original ``data.h5`` file only if it has not done
it before. Otherwise it loads it from a faster-to-read format from the ``analysis.h5``
file.
"""

import os
import numpy as np
import matplotlib.pyplot as plt
from bead_state_model.data_reader import DataReader
from actomyosin_analyser.analysis.analyser import Analyser

simulation_indices = [0, 1, 2]

fig, ax = plt.subplots(1, len(simulation_indices))

for i, sim_index in enumerate(simulation_indices):

    sim_dir = os.path.join('..', 'simulations', f'out{sim_index:04}')
    # instead of fixed name, provide unformatted string part_{:04} when dealing
    # with split simulation data
    dr = DataReader(os.path.join(sim_dir, 'data.h5'))
    # to Analyser class, provide a data_reader and the path to the output file
    a = Analyser(dr, os.path.join(sim_dir, 'analysis.h5'))

    # there is only one non-filament particle in these simulations:
    # the large bead for microrheo measurements. select it with index 0
    bead_traj = a.get_trajectories_non_filament()[:, 0]
    # To read the trajectories of the filaments (or rather the beads that represent
    # the filament) use the method a.get_trajectories_filaments() instead.

    ax_i = ax[i]
    ax_i.scatter(bead_traj[:, 0], bead_traj[:, 1], s=0.5, cmap='viridis',
                 label='sim {:02}'.format(sim_index), c=np.arange(len(bead_traj)))
    ax_i.legend()
    ax_i.set_aspect('equal')

plt.show()
