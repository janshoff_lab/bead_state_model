import argparse
import json
import os
import time
from multiprocessing import Pool
from typing import Dict, Any, Sequence, List

import pandas as pd
import numpy as np
import readdy
from readdy import ReactionDiffusionSystem

from bead_state_model import Simulation, BaseSetupHandler

_ParamSet = Dict[str, Any]


def main(n_processes: int):

    network_index = pd.read_csv('initial_states/index.csv')
    network_index = np.array(network_index['index']).astype(int)

    parameter_sets = _generate_parameter_sets(network_index)

    pool = Pool(n_processes)
    results = pool.map(run_equilibration, parameter_sets)

    r = np.array(results)
    # store some information on run time of each simulation
    np.savetxt('duration_of_equilibrations.txt', r, header='# run_time')


def run_equilibration(params: _ParamSet) -> float:

    params = params.copy()

    idx = params.pop('index')
    idx_network = idx

    print('running equilibration {:03}'.format(idx))

    t_start = time.time()

    output_folder = params.pop('out_dir')
    obs_interval = params.pop('observation_interval')
    n_steps = params.pop('n_steps')
    dt = params.pop('dt')
    box = np.array(params['box_size'])
    mr_bead_parameters = params.pop('microrheology_bead')

    folder_network = 'initial_states/network{:03}'.format(idx_network)
    file_beads = os.path.join(folder_network, 'beads.txt')
    file_config = os.path.join(folder_network, 'config.json')

    with open(file_config, 'rt') as fp:
        config = json.load(fp)

    bead_position = np.array(config['acceptance_rate_handler']['bead_position']) - box / 2
    bead_diffusion_const = 0.5 / mr_bead_parameters['radius']

    mr_bead_setup_handler = MRBeadSetupHandler(mr_bead_parameters['radius'],
                                               mr_bead_parameters['k_repulsion'])

    s = Simulation(
        output_folder=output_folder,
        parameters=params,
        non_filament_particles={'bead': bead_diffusion_const},
        interaction_setup_handler=mr_bead_setup_handler
    )

    s.add_non_filament_particles(bead=bead_position[np.newaxis, :])
    s.add_filaments(file_beads)

    s.run(n_steps, dt, obs_interval, mute=True)

    t_end = time.time()
    t = t_end - t_start
    print('done with equilibration {}'.format(idx))

    return t


class MRBeadSetupHandler(BaseSetupHandler):

    def __init__(self, radius: float, k_repulsion: float):
        self._radius = radius
        self._k_repulsion = k_repulsion

    def __call__(self, system: ReactionDiffusionSystem):
        for other in ['core', 'head', 'tail', 'motor']:
            system.potentials.add_harmonic_repulsion(
                "bead", other,
                force_constant=self._k_repulsion,
                interaction_distance=.5 + self._radius
            )

    @staticmethod
    def from_config_dict(d: Dict[str, Any]) -> 'MRBeadSetupHandler':
        return MRBeadSetupHandler(**d)

    def to_config_dict(self) -> Dict[str, Any]:
        return {'radius': self._radius, 'k_repulsion': self._k_repulsion}


def _generate_parameter_sets(
        network_index: Sequence[int]
) -> List[_ParamSet]:

    out_dir = 'initial_states_equilibrated'
    os.makedirs(out_dir, exist_ok=True)
    with open('default_params.json', 'rt') as fh:
        default_config = json.load(fh)

    parameter_sets = []
    for i in network_index:
        # default parameter are read from config file
        params = default_config.copy()

        # add parameters for run_equilibration function
        params['index'] = int(i)
        params['out_dir'] = os.path.join(out_dir, 'network{:03}'.format(i))

        # most important parameters to overwrite for equilibration:
        # number of steps (to have comparably short simulations),
        # time step (to safely remove overlaps of particles)
        params['n_steps'] = int(6e3)
        params['observation_interval'] = int(2e3)
        params['dt'] = 1e-6

        # don't allow motor binding in equilibration
        params['rate_motor_bind'] = 0.0

        parameter_sets.append(params)

    return parameter_sets




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('number_of_processes', type=int)
    args = parser.parse_args()

    main(args.number_of_processes)
