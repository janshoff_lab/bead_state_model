FROM debian:11.1

RUN apt update
RUN apt -y install wget gcc

WORKDIR /miniconda
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-py38_4.10.3-Linux-x86_64.sh
RUN bash Miniconda3-py38_4.10.3-Linux-x86_64.sh -b -p /miniconda -f
RUN eval "$(/miniconda/bin/conda shell.bash hook)" \
    && conda init \
    && conda config --add channels conda-forge \
    && conda install hdf5 h5py \
    && conda install -c readdy readdy \
    && pip install bead_state_model==2.2.3 actomyosin_analyser==1.18.8 flex-plot

CMD bash -c "while true; do sleep 1; done"