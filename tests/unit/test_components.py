import numpy as np
from bead_state_model.components import Filament


def test_filament_to_readdy_species_list():
    links = np.full((20, 3), -1, dtype=int)

    # define links for two linear filaments with 10 beads each:
    links[1:10, 0] = np.arange(9)
    links[:9, 1] = np.arange(1, 10)

    links[11:, 0] = np.arange(9) + 10
    links[10:19, 1] = np.arange(1, 10) + 10

    filaments = []
    for id_, tail in enumerate([0, 10]):
        filaments.append(Filament(id_, tail, links))

    for fil in filaments:
        # no links (motors) between filaments defined
        assert len(fil.motors) == 0
        slist = fil.to_readdy_species_list()
        assert len(slist) == 10
        assert slist[0] == 'tail'
        assert slist[-1] == 'head'
        for i in range(1, 9):
            assert slist[i] == 'core'

    # now add connection between filaments
    links[4, 2] = 18
    links[18, 2] = 4

    filaments = []
    for id_, tail in enumerate([0, 10]):
        filaments.append(Filament(id_, tail, links))

    motor_pos = [4, 8]
    for fil, m in zip(filaments, motor_pos):
        slist = fil.to_readdy_species_list()
        assert len(slist) == 10
        assert slist[0] == 'tail'
        assert slist[-1] == 'head'
        for i in range(1, 9):
            if i == m:
                assert slist[i] == 'motor'
            else:
                assert slist[i] == 'core'
