from typing import Tuple
import numpy as np
from bead_state_model.data_reader import (revert_minimum_image_projection,
                                          get_minimum_image_boundary_crossings)


def test_get_minimum_image_boundary_crossings():
    box, traj_both_particles = _construct_trajectory()

    displacements = traj_both_particles[1:] - traj_both_particles[:-1]
    boundary_crossings_both_particles = get_minimum_image_boundary_crossings(displacements, box)
    boundary_crossings = boundary_crossings_both_particles[:, 0]

    assert (boundary_crossings[:3] == 0).all()
    assert boundary_crossings[3, 0] == 0
    assert boundary_crossings[3, 1] == 1
    assert boundary_crossings[3, 2] == 1
    assert boundary_crossings[4, 0] == 1
    assert (boundary_crossings[4, 1:] == 0).all()
    assert (boundary_crossings[5] == 0).all()
    assert boundary_crossings[6, 0] == -1
    assert (boundary_crossings[6, 1:] == 0).all()
    assert boundary_crossings[7, 0] == 1
    assert (boundary_crossings[7, 1:] == 0).all()
    assert boundary_crossings[8, 2] == 1
    assert (boundary_crossings[8, :2] == 0).all()
    assert (boundary_crossings[9] == 0).all()
    assert boundary_crossings[10, 0] == 0
    assert boundary_crossings[10, 1] == 1
    assert boundary_crossings[10, 2] == 0
    assert boundary_crossings[11, 0] == -1
    assert boundary_crossings[11, 1] == 0
    assert boundary_crossings[11, 2] == 1
    assert (boundary_crossings[12] == 0).all()
    assert boundary_crossings[13, 0] == 0
    assert boundary_crossings[13, 1] == -1
    assert boundary_crossings[13, 2] == 0

    boundary_crossings = boundary_crossings_both_particles[:, 1]
    assert (boundary_crossings[:4] == 0).all()
    assert boundary_crossings[4, 0] == 0
    assert boundary_crossings[4, 1] == 1
    assert boundary_crossings[4, 2] == -1
    assert boundary_crossings[5, 0] == 1
    assert (boundary_crossings[5, 1:] == 0).all()
    assert (boundary_crossings[6] == 0).all()
    assert boundary_crossings[7, 0] == -1
    assert (boundary_crossings[7, 1:] == 0).all()
    assert boundary_crossings[8, 0] == 1
    assert (boundary_crossings[8, 1:] == 0).all()
    assert boundary_crossings[9, 2] == -1
    assert (boundary_crossings[9, :2] == 0).all()
    assert (boundary_crossings[10] == 0).all()
    assert boundary_crossings[11, 0] == 0
    assert boundary_crossings[11, 1] == 1
    assert boundary_crossings[11, 2] == 0
    assert boundary_crossings[12, 0] == -1
    assert boundary_crossings[12, 1] == 0
    assert boundary_crossings[12, 2] == -1
    assert (boundary_crossings[13] == 0).all()


def test_revert_minimum_image_projection():
    box, traj_both_particles = _construct_trajectory()
    reverted_traj_both_particles = revert_minimum_image_projection(traj_both_particles, box)

    traj = traj_both_particles[:, 0]
    reverted_traj = reverted_traj_both_particles[:, 0]

    assert (reverted_traj[:4] == traj[:4]).all()
    assert (reverted_traj[4] == (traj[4] + np.array([0, box[1], box[2]]))).all()
    assert (reverted_traj[5] == (traj[5] + np.array([box[0], box[1], box[2]]))).all()
    assert (reverted_traj[6] == (traj[6] + np.array([box[0], box[1], box[2]]))).all()
    assert (reverted_traj[7] == (traj[7] + np.array([0.0, box[1], box[2]]))).all()
    assert (reverted_traj[8] == (traj[8] + np.array([box[0], box[1], box[2]]))).all()
    assert (reverted_traj[9] == (traj[9] + np.array([box[0], box[1], 2*box[2]]))).all()
    assert (reverted_traj[10] == (traj[10] + np.array([box[0], box[1], 2 * box[2]]))).all()
    assert (reverted_traj[11] == (traj[11] + np.array([box[0], 2*box[1], 2 * box[2]]))).all()
    assert (reverted_traj[12] == (traj[12] + np.array([0, 2 * box[1], 3 * box[2]]))).all()
    assert (reverted_traj[13] == (traj[13] + np.array([0, 2 * box[1], 3 * box[2]]))).all()
    assert (reverted_traj[14] == (traj[14] + np.array([0, 1 * box[1], 3 * box[2]]))).all()

    traj = traj_both_particles[:, 1]
    reverted_traj = reverted_traj_both_particles[:, 1]
    assert (reverted_traj[:5] == traj[:5]).all()
    assert (reverted_traj[5] == (traj[5] + np.array([0, box[1], -box[2]]))).all()
    assert (reverted_traj[6] == (traj[6] + np.array([box[0], box[1], -box[2]]))).all()
    assert (reverted_traj[7] == (traj[7] + np.array([box[0], box[1], -box[2]]))).all()
    assert (reverted_traj[8] == (traj[8] + np.array([0.0, box[1], -box[2]]))).all()
    assert (reverted_traj[9] == (traj[9] + np.array([box[0], box[1], -box[2]]))).all()
    assert (reverted_traj[10] == (traj[10] + np.array([box[0], box[1], -2 * box[2]]))).all()
    assert (reverted_traj[11] == (traj[11] + np.array([box[0], box[1], -2 * box[2]]))).all()
    assert (reverted_traj[12] == (traj[12] + np.array([box[0], 2 * box[1], -2 * box[2]]))).all()
    assert (reverted_traj[13] == (traj[13] + np.array([0, 2 * box[1], -3 * box[2]]))).all()
    assert (reverted_traj[14] == (traj[14] + np.array([0, 2 * box[1], -3 * box[2]]))).all()


def _construct_trajectory() -> Tuple[np.ndarray, np.ndarray]:
    box = np.ones(3) * 10
    traj = np.zeros((15, 3))

    traj[0] = np.zeros(3)
    traj[1] = [1.5, 1.5, 1.5]
    traj[2] = [3, 3, 3.0]
    traj[3] = [4.4, 4.4, 4.5]
    traj[4] = [4.4, -4.4, -4]  # moves across y and z
    traj[5] = [-4.4, -4.4, -2.5]  # moves across x
    traj[6] = [-4.4, -3, 0.0]
    traj[7] = [4.6, -1.6, 2.5]  # moves across -x
    traj[8] = [-4.4, 0.6, 4.9]  # moves across x
    traj[9] = [-4.4, 3.8, -3.0]  # moves across z
    traj[10] = [-4.4, 4.6, -0.2]
    traj[11] = [-4.4, -4.6, 2.9]  # moves across y
    traj[12] = [4.6, -4.6, -4.9]  # moves across -x and z
    traj[13] = [3.4, -4.6, -4.0]
    traj[14] = [0.4, 4.8, -3.0]  # moves across -y

    # to get shape (n_steps, n_particles, 3) as in a simulation,
    # I simply duplicate the trajectory shifted by one step
    traj_2_particles = np.zeros((15, 2, 3))
    traj_2_particles[:, 0] = traj
    traj_2_particles[1:, 1] = traj[:-1]
    # but I make z move the other way, to actually get to multiple
    # boundary crossings in a negative direction
    traj_2_particles[:, 1, 2] = (traj_2_particles[:, 1, 2] + 5) * -1 + 5

    return box, traj_2_particles
