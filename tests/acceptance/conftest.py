from dataclasses import dataclass
import os
from typing import Dict, Optional

import numpy as np

from bead_state_model import Simulation, SimulationParameters, BaseSetupHandler
from bead_state_model.data_reader import DataReader
from bead_state_model.network_assembly.create_network_simple import CreateNetworkSimple



def create_initial_state(
        init_dir: str,
        box: np.ndarray,
        n_beads_per_filament: int,
        n_filaments: int
):
    os.mkdir(init_dir)
    cns = CreateNetworkSimple(box, k_bend=26.0, k_stretch=20.0,
                              n_beads_per_filament=n_beads_per_filament,
                              n_filaments=n_filaments)
    cns.run(verbose=False)
    cns.write(init_dir)


def run_equilibration(
        init_dir: str, equil_dir: str, kernel: str,
        box: np.ndarray,
        n_beads_per_filament: int,
        n_filaments: int
):
    run_simulation(init_dir, equil_dir, kernel,
                   dt=0.006,
                   n_steps=10000,
                   observation_interval=10000,
                   box=box,
                   n_beads_per_filament=n_beads_per_filament,
                   n_filaments=n_filaments)


def run_simulation(
        initial_state_dir: str,
        out_dir: str,
        kernel: str,
        dt: float,
        n_steps: int,
        observation_interval: int,
        box: np.ndarray,
        n_beads_per_filament: int,
        n_filaments: int,
        external_particles: Optional[Dict[str, 'ExternalParticle']]=None,
        external_particle_setup_handler: Optional[BaseSetupHandler]=None
):
    os.mkdir(out_dir)
    simulation = _setup_simulation(
        initial_state_dir,
        out_dir, kernel,
        box,
        n_beads_per_filament,
        n_filaments,
        external_particles=external_particles,
        external_particle_setup_handler=external_particle_setup_handler
    )
    simulation.run(n_steps, dt, observation_interval, mute=True)


@dataclass
class ExternalParticle:
    coords: np.ndarray
    diffusion_const: float


def _setup_simulation(
        initial_state_dir: str,
        out_dir: str, kernel: str,
        box: np.ndarray,
        n_beads_per_filament: int,
        n_filaments: int,
        external_particles: Optional[Dict[str, 'ExternalParticle']],
        external_particle_setup_handler: Optional[BaseSetupHandler]
) -> Simulation:

    params = SimulationParameters(box_size=box,
                                  k_bend=26.,
                                  k_stretch=20.,
                                  n_beads_max=n_beads_per_filament * n_filaments)
    if external_particles is None:
        non_fil_particles = None
    else:
        non_fil_particles = {
            key: external_particles[key].diffusion_const for key in external_particles
        }
    simulation = Simulation(
        out_dir, params, kernel,
        non_filament_particles=non_fil_particles,
        interaction_setup_handler=external_particle_setup_handler
    )

    if external_particles is not None:
        external_particle_kwargs = {
            key: external_particles[key].coords for key in external_particles
        }
        simulation.add_non_filament_particles(**external_particle_kwargs)

    if 'beads.txt' in os.listdir(initial_state_dir):
        simulation.add_filaments(os.path.join(initial_state_dir, 'beads.txt'))
    else:
        simulation.add_filaments(os.path.join(initial_state_dir, 'data.h5'))
    return simulation


def read_trajectories(out_dir: str) -> np.ndarray:
    dr = DataReader(os.path.join(out_dir, 'data.h5'))
    traj = dr.read_particle_positions()
    return traj