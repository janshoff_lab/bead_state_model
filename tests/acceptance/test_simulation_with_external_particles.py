import os
from tempfile import TemporaryDirectory
from typing import Dict, Any, Optional

import numpy as np
import readdy
from readdy import ReactionDiffusionSystem

from bead_state_model import BaseSetupHandler
from bead_state_model.data_reader import DataReader
from tests.acceptance.conftest import (create_initial_state,
                                       run_simulation,
                                       ExternalParticle)

N_FILAMENTS = 10
N_BEADS_PER_FILAMENT = 15
DT = 0.006
BOX = np.array([30., 30., 30.])


def test_run_simulation_with_one_external_particle():

    with TemporaryDirectory() as tempdir:
        init_dir = os.path.join(tempdir, 'init')
        out_dir = os.path.join(tempdir, 'out')

        create_initial_state(init_dir, BOX, N_BEADS_PER_FILAMENT, N_FILAMENTS)
        run_simulation(
            init_dir, out_dir, kernel='SingleCPU',
            dt=DT, n_steps=100, observation_interval=1,
            box=BOX,
            n_beads_per_filament=N_BEADS_PER_FILAMENT,
            n_filaments=N_FILAMENTS,
            external_particle_setup_handler=SetupSomeParticle(),
            external_particles={
                'some_particle': ExternalParticle(
                    coords=np.array([0.0, 0.0, 0.0])[np.newaxis, :],
                    diffusion_const=0.5
                )
            }
        )
        dr = DataReader(os.path.join(out_dir, 'data.h5'))
        assert dr.get_n_non_filament_particles() == 1
        traj = dr.read_particle_positions()
        assert traj.shape[1] == N_FILAMENTS * N_BEADS_PER_FILAMENT + 1


class SetupSomeParticle(BaseSetupHandler):

    def __call__(self, system: ReactionDiffusionSystem):
        for other in ['core', 'head', 'tail', 'motor']:
            system.potentials.add_harmonic_repulsion(
                "some_particle", other,
                force_constant=200.,
                interaction_distance=1.5
            )

    @staticmethod
    def from_config_dict(d: Optional[Dict[str, Any]]=None) -> 'BaseSetupHandler':
        return SetupSomeParticle()

    def to_config_dict(self) -> Dict[str, Any]:
        return {}
