from typing import Tuple, Any, List
import os
import numpy as np
import readdy
from bead_state_model.system_setup import (system_setup,
                                           simulation_setup,
                                           _add_filament,
                                           RDSystem)
from bead_state_model.components import Filament
from bead_state_model.filament_handler import FilamentHandler

Topology = Any


def test_motor_binding():
    """
    test_reactions.test_motor_binding
    In this test 4 filaments are created to form a square, i.e. filaments
    are touching in the corners of this square.
    If the min_network_distance parameter is chosen correctly, the filaments
    will form a topology that is connected in all 4 corners.
    If the min_network_distance parameter is chosen too large, this will not happen
    and only 3 connections will be formed.
    """
    kernels = ["SingleCPU", "CPU"]
    for kernel in kernels:
        _test_motor_binding_single_kernel(kernel)


def _test_motor_binding_single_kernel(kernel):
    n_steps = 30
    dt = 1e-3

    dist_matching = 15  # o.k. for full connection
    dist_too_large = 16  # too large for full connection

    syst_matching, sim_matching = _setup_filaments_for_motor_binding(kernel, dist_matching)
    syst_too_large, sim_too_large = _setup_filaments_for_motor_binding(kernel, dist_too_large)
    sim_matching.output_file = 'tmp_sim_matching.h5'
    sim_too_large.output_file = 'tmp_sim_too_large.h5'
    for sim in [sim_matching, sim_too_large]:
        if os.path.exists(sim.output_file):
            os.remove(sim.output_file)
        sim.observe.topologies(1)
        sim.run(n_steps, dt)
    file_m = readdy.Trajectory(sim_matching.output_file)
    file_l = readdy.Trajectory(sim_too_large.output_file)
    times, top_records_m = file_m.read_observable_topologies()
    times, top_records_l = file_l.read_observable_topologies()
    assert 1 == len(top_records_m[-1])
    assert 1 == len(top_records_l[-1])
    tr_m = top_records_m[-1][0]  # select the only record of the last time step
    tr_l = top_records_l[-1][0]
    # assert initial filament links
    for i in range(5):
        assert (i, i + 1) in tr_m.edges
        assert (i, i + 1) in tr_l.edges
        assert (i + 6, i + 7) in tr_m.edges
        assert (i + 6, i + 7) in tr_l.edges
        assert (i + 12, i + 13) in tr_m.edges
        assert (i + 18, i + 19) in tr_l.edges
    assert 23 == len(tr_l.edges)
    assert 24 == len(tr_m.edges)
    # Prepare check of crosslinks.
    # Unfortunately, topologies are not always added in the
    # same order, so I have to check all combination of
    # possible crosslink positions here.
    link_points = [1, 4, 7, 10, 13, 16, 19, 22]
    in_same_filament = [(1, 4), (7, 10), (13, 16), (19, 22)]
    combinations = []
    for i in range(len(link_points) - 1):
        for j in range(i + 1, len(link_points)):
            t = (link_points[i], link_points[j])
            if t in in_same_filament:
                continue
            combinations.append(t)
            combinations.append((t[1], t[0]))
    count = 0
    for cl in combinations:
        if cl in tr_m.edges:
            count += 1
    assert count == 4, "count is {}".format(count)
    # the system with too large min distance should be connected in only
    # 3 places
    count = 0
    for cl in combinations:
        if cl in tr_l.edges:
            count += 1
    assert count == 3, "count is {}".format(count)
    os.remove(sim_matching.output_file)
    os.remove(sim_too_large.output_file)


def test_max_n_motors():
    # limit number of motors to 4 in simulation,
    # this means only 2 link pairs can form,
    # and of the 4 initial topologies (4 separate filaments),
    # 2 will remain after the simulation
    n_max_motors = 4
    for kernel in ["SingleCPU", "CPU"]:
        system, simulation = _setup_filaments_for_motor_binding(kernel, 6, n_max_motors)

        simulation.run(n_steps=30, timestep=1.0)

        assert 2 == len(simulation.current_topologies)


def test_motor_unbind():
    n_steps = 20
    dt = 1e-3
    n_beads = 10
    kernels = ["SingleCPU", "CPU"]
    for kernel in kernels:
        system, simulation = _setup_filaments_for_motor_unbinding(kernel, n_beads)
        simulation.output_file = 'tmp_sim_motor_unbind.h5'
        if os.path.exists(simulation.output_file):
            os.remove(simulation.output_file)
        simulation.observe.topologies(1)
        simulation.run(n_steps, dt)

        file = readdy.Trajectory(simulation.output_file)
        times, top_records = file.read_observable_topologies()

        assert 1 == len(top_records[0])
        assert 4 == len(top_records[-1])

        tr_last = top_records[-1]
        for i in range(n_beads-1):
            for j in range(4):
                b = _edge_in_any_topology((j*n_beads + i, j*n_beads + i+1), tr_last) or _edge_in_any_topology((j*n_beads + i+1, j*n_beads+i), tr_last)
                assert b


def test_motor_step():
    n_steps = 100
    n_beads = 10  # choose << n_steps, so that the motor can reach the end
    dt = 5e-3
    kernels = ["SingleCPU", "CPU"]
    for kernel in kernels:
        system, simulation = _setup_2_filaments_1_motor(kernel)
        simulation.output_file = 'tmp_motor_step.h5'
        if os.path.exists(simulation.output_file):
            os.remove(simulation.output_file)
        simulation.observe.topologies(1)
        simulation.observe.particles(1)
        simulation.run(n_steps, dt)

        data = readdy.Trajectory(simulation.output_file)
        times, top_records = data.read_observable_topologies()
        time, types, ids, positions = data.read_observable_particles()

        assert 1 == len(top_records[-1])
        tr = top_records[-1][0]
        assert 2*n_beads-1 == len(tr.edges)
        assert (n_beads-2, 2*n_beads-2) in tr.edges or (2*n_beads-2, n_beads-2) in tr.edges
    del data
    os.remove(simulation.output_file)


def test_motor_step_many_motors_on_filaments():
    n_steps = 200
    n_beads = 8 # choose << n_steps, so that the motor can reach the end
    dt = 5e-3
    kernels = ["SingleCPU", "CPU"]
    for kernel in kernels:
        system, simulation = _setup_2_filaments_n_min_1_motors(kernel, n_beads)
        simulation.output_file = 'tmp_motor_step.h5'
        if os.path.exists(simulation.output_file):
            os.remove(simulation.output_file)
        simulation.observe.topologies(1)
        simulation.observe.particles(1)
        simulation.run(n_steps, dt)

        data = readdy.Trajectory(simulation.output_file)
        times, top_records = data.read_observable_topologies()
        time, types, ids, positions = data.read_observable_particles()
        assert 1 == len(top_records[-1])
        tr = top_records[-1][0]
        assert 2*n_beads-2 + n_beads - 3 == len(tr.edges)
        assert (1, n_beads+1) not in tr.edges
        print(tr.edges)
        for j in range(2, n_beads-2):
            print(n_beads-j, 2*n_beads-j)
            assert (n_beads-j, 2*n_beads-j) in tr.edges or (2*n_beads-j, n_beads-j) in tr.edges

    del data
    os.remove(simulation.output_file)


def test_filament_growth():
    n_steps = 200
    dt = 5e-3
    kernels = ["SingleCPU", "CPU"]
    for kernel in kernels:
        box = [30., 30., 30.]
        system, filament_handler = system_setup(
            box,
            n_beads_max=1000,
            diffusion_const=0.0,
            rate_attach=1e10,
        )

        simulation  = simulation_setup(system, filament_handler, kernel)
        coords = np.zeros((3, 3))
        coords[:, 0] = [0, 1, 2]
        _add_filament(simulation, coords)

        links = np.full((3, 3), -1, dtype=int)
        links[:, 1] = [1, 2, -1]
        links[:, 0] = [-1, 0, 1]

        id_ = 0
        filament = Filament(id_, 0, links)
        filament_handler.initialize([filament], links)

        simulation.output_file = 'tmp_filament_growth.h5'
        if os.path.exists(simulation.output_file):
            os.remove(simulation.output_file)                    
        simulation.observe.topologies(1)
        simulation.observe.particles(1)
        simulation.run(n_steps, dt)

        data = readdy.Trajectory(simulation.output_file)
        times, top_records = data.read_observable_topologies()
        time, types, ids, positions = data.read_observable_particles()

        assert len(top_records[-1][0].particles) > 3
        t = data.particle_types['head']
        assert 1 == (types[-1] == t).sum()
        t = data.particle_types['tail']
        assert 1 == (types[-1] == t).sum()

        os.remove(simulation.output_file)


def test_filament_dissolve():
    n_beads = 10
    n_steps = 20
    dt = 5e-3
    kernels = ["SingleCPU", "CPU"]
    for kernel in kernels:
        box = [30., 30., 30.]
        system, filament_handler = system_setup(
            box,
            n_beads_max=1000,
            diffusion_const=0.0,
            rate_detach=1e10
        )

        simulation = simulation_setup(system, filament_handler, kernel)

        coords = np.zeros((n_beads, 3))
        coords[:, 0] = np.arange(-n_beads//2, n_beads//2)

        _add_filament(simulation, coords)

        links = np.full((n_beads, 3), -1, dtype=int)
        links[:n_beads-1, 1] = np.arange(1, n_beads)
        links[1:n_beads, 0] = np.arange(n_beads-1)

        id_ = 0
        filament = Filament(id_, 0, links)
        filament_handler.initialize([filament], links)

        simulation.output_file = 'tmp_filament_dissolve.h5'
        if os.path.exists(simulation.output_file):
            os.remove(simulation.output_file)                    
        simulation.observe.particles(1)
        simulation.run(n_steps, dt)

        data = readdy.Trajectory(simulation.output_file)
        time, types, ids, positions = data.read_observable_particles()

        assert len(types[0]) == n_beads
        t = data.particle_types['head']
        assert 1 == (types[-1] == t).sum()
        t = data.particle_types['tail']
        assert 1 == (types[-1] == t).sum()
        t = data.particle_types['core']
        assert 1 == (types[-1] == t).sum()

        os.remove(simulation.output_file)


def test_filament_handler_with_external_particles():
    n_steps = 20
    dt = 1e-3
    n_beads = 10
    kernels = ["SingleCPU", "CPU"]
    for kernel in kernels:
        system, simulation, filament_handler = _setup_full_reactions(kernel)
        simulation.output_file = 'tmp_sim.h5'
        if os.path.exists(simulation.output_file):
            os.remove(simulation.output_file)
        simulation.record_trajectory(1)
        simulation.observe.topologies(1)
        simulation.run(n_steps, dt)
        os.remove(simulation.output_file)


def _edge_in_any_topology(edge: Tuple[int, int], topologies: List[Topology]) -> bool:
    """

    :param edge: Does not contain vertex indices directly, because vertices are not
                 necessarily in matching order after an unbind
    """
    for top in topologies:
        if edge[0] not in top.particles:
            continue
        vid0 = top.particles.index(edge[0])
        vid1 = top.particles.index(edge[1])
        edge_in_top_indices = (min(vid0, vid1), max(vid0, vid1))
        if edge_in_top_indices in top.edges or (edge_in_top_indices[1], edge_in_top_indices[0]) in top.edges:
            return True
    return False


def _setup_filaments_for_motor_binding(
        kernel: str, min_network_distance: int,
        n_max_motors: int=None
) -> Tuple[RDSystem, readdy.Simulation]:
    system, filament_handler = system_setup(
        [30., 30., 30.],
        min_network_distance=min_network_distance,
        diffusion_const=0.0,
        rate_motor_bind=1e10,
        n_beads_max=100,
        n_max_motors=n_max_motors
    )
    simulation = simulation_setup(system, filament_handler, kernel)
    _setup_4_separate_filaments(simulation, filament_handler)
    return system, simulation


def _setup_4_separate_filaments(simulation: readdy.Simulation,
                                filament_handler):

    coords1 = np.array([
        [-1, 0, 0],
        [0, 0, 0],
        [1, 0, 0],
        [2, 0, 0],
        [3, 0, 0],
        [4, 0, 0],
    ])

    coords2 = np.array([
        [3, 1, 1],
        [3, 0, 1],
        [3, -1, 1],
        [3, -2, 1],
        [3, -3, 1],
        [3, -4, 1],
    ])

    coords3 = np.array([
        [4, -3, 0],
        [3, -3, 0],
        [2, -3, 0],
        [1, -3, 0],
        [0, -3, 0],
        [-1, -3, 0],
    ])

    coords4 = np.array([
        [0, -4, 1],
        [0, -3, 1],
        [0, -2, 1],
        [0, -1, 1],
        [0, 0, 1],
        [0, 1, 1],
    ])

    n = np.array([len(coords1), len(coords2), len(coords3), len(coords4)])
    links = np.full((n.sum(), 3), -1, dtype=int)
    for i in range(len(n)):
        low = n[:i].sum()
        high = n[:i + 1].sum()
        ni = n[i]
        links[low: high - 1, 1] = low + np.arange(1, ni)
        links[low + 1: high, 0] = low + np.arange(ni - 1)

    filaments = []
    for id_, i in enumerate(range(len(n))):
        tail_idx = n[:i].sum()
        fil = Filament(id_, tail_idx, links)
        filaments.append(fil)

    topologies = []
    for coords in [coords1, coords2, coords3, coords4]:
        topologies.append(_add_filament(simulation, coords))

    filament_handler.initialize(filaments, links)


def _setup_full_reactions(
        kernel: str,
        ) -> Tuple[RDSystem, readdy.Simulation, FilamentHandler]:
    system, filament_handler = system_setup(
        [30., 30., 30.],
        min_network_distance=3,
        diffusion_const=1.0,
        rate_motor_step=1e3,
        rate_motor_bind=1e3,
        rate_motor_unbind=1e3,
        n_beads_max=100,
        rate_attach=1e3,
        rate_detach=1e3
    )

    system.add_species("other", 0.1)
    simulation = simulation_setup(system, filament_handler, kernel)

    filament_handler.add_non_filament_particle("other", np.random.rand(3)*30.-15)
    system.potentials.add_harmonic_repulsion("other", "core", 0.5, 1)

    _setup_4_separate_filaments(simulation, filament_handler)

    return system, simulation, filament_handler


def _setup_filaments_for_motor_unbinding(
        kernel: str,
        n_beads: int
    ) -> Tuple[RDSystem, readdy.Simulation]:

    system, filament_handler = system_setup(
        [30., 30., 30.],
        diffusion_const=0,
        rate_motor_step=0,
        rate_motor_unbind=1e10,
        n_beads_max=100
    )

    simulation = simulation_setup(system, filament_handler, kernel)

    coords1 = np.array([
        [-1, 0, 0],
        [0, 0, 0],
        [1, 0, 0],
        [2, 0, 0],
        [3, 0, 0],
        [4, 0, 0],
    ])

    coords1 = np.zeros((n_beads, 3))
    coords1[:, 0] = np.arange(n_beads)-1

    coords2 = np.zeros((n_beads, 3))
    coords2[:, 0] = n_beads-3
    coords2[:, 1] = 1 - np.arange(n_beads)
    coords2[:, 2] = 1

    coords3 = np.zeros((n_beads, 3))
    coords3[:, 0] = n_beads - 1 - np.arange(n_beads)
    coords3[:, 1] = -1 * (n_beads-3)

    coords4 = np.zeros((n_beads, 3))
    coords4[:, 1] = -n_beads + 1 + np.arange(n_beads)
    coords4[:, 2] = 1

    n = np.array([len(coords1), len(coords2), len(coords3), len(coords4)])
    links = np.full((n.sum(), 3), -1, dtype=int)
    for i in range(len(n)):
        low = n[:i].sum()
        high = n[:i+1].sum()
        ni = n[i]
        links[low: high-1, 1] = low + np.arange(1, ni)
        links[low+1: high, 0] = low + np.arange(ni-1)

    link_points = [1, n_beads-2,
                   n_beads+1, 2*n_beads-2,
                   2*n_beads+1, 3*n_beads-2,
                   3*n_beads+1, 4*n_beads-2]
    combinations = []
    for i in range(4):
        idx1 = link_points[2*i]
        idx2 = link_points[2*i-1]
        combinations.append((min(idx1, idx2), max(idx1, idx2)))


    for cl in combinations:
        links[cl[0], 2] = cl[1]
        links[cl[1], 2] = cl[0]

    filaments = []
    for i in range(4):
        tail_idx = i*n_beads
        fil = Filament(i, tail_idx, links)
        filaments.append(fil)

    species_list = []
    for fil in filaments:
        species_list += fil.to_readdy_species_list()

    for i in range(4):
        species_list[i*n_beads + 1] = 'motor'
        species_list[(i+1)*n_beads - 2] = 'motor'

    coords = np.empty((4 * n_beads, 3))
    for i, c in enumerate([coords1, coords2, coords3, coords4]):
        coords[i * n_beads: (i + 1) * n_beads] = c

    top = simulation.add_topology('filament', species_list, coords)
    graph = top.get_graph()
    for i in range(4):
        for j in range(n_beads-1):
            graph.add_edge(i*n_beads+j, i*n_beads+j+1)
    for cl in combinations:
        graph.add_edge(*cl)


    filament_handler.initialize(filaments, links)

    return system, simulation


def _setup_2_filaments_1_motor(
        kernel: str) -> Tuple[RDSystem, readdy.Simulation]:

    system, filament_handler = system_setup(
        [30., 30., 30.],
        n_beads_max=50,
        min_network_distance=1,
        diffusion_const=0.1,
        rate_motor_step=1e15,
        rate_motor_bind=0
    )

    simulation = simulation_setup(system, filament_handler, kernel)
    
    n_beads = 10

    init_pos = np.zeros((2*n_beads, 3))

    init_pos[:n_beads, 0] = np.arange(-1, n_beads-1)
    init_pos[n_beads:, 2] = 1
    init_pos[n_beads:, 1] = np.arange(-1, n_beads-1)

    links  = np.full((len(init_pos), 3), -1, dtype=int)
    links[:n_beads-1, 1] = np.arange(1, n_beads)
    links[n_beads: 2*n_beads-1, 1] = n_beads + np.arange(1, n_beads)
    links[1: n_beads, 0] = np.arange(n_beads-1)
    links[n_beads+1: 2*n_beads, 0] = n_beads + np.arange(n_beads-1)
    links[1, 2] = n_beads+1
    links[n_beads+1, 2] = 1

    filaments = [Filament(0, 0, links), Filament(1, n_beads, links)]

    fil = ['tail', 'motor'] + ['core'] * (n_beads-3) + ['head']

    top = simulation.add_topology(
        "filament", fil + fil, init_pos
    )

    graph = top.get_graph()
    for j in range(n_beads-1):
        graph.add_edge(j, j+1)
        graph.add_edge(j+n_beads, j+1+n_beads)
    graph.add_edge(1, 1+n_beads)

    filament_handler.initialize(filaments, links)

    return system, simulation


def _setup_2_filaments_n_min_1_motors(kernel: str, n_beads: int=10) -> Tuple[RDSystem, readdy.Simulation]:
    system, filament_handler = system_setup(
        [30., 30., 30.],
        n_beads_max=50,
        min_network_distance=1,
        diffusion_const=0.1,
        rate_motor_step=1e15,
        rate_motor_bind=0
    )

    simulation = simulation_setup(system, filament_handler, kernel)

    init_pos = np.zeros((2 * n_beads, 3))

    init_pos[:n_beads, 0] = np.arange(-1, n_beads - 1)
    init_pos[n_beads:, 2] = 1
    init_pos[n_beads:, 0] = np.arange(-1, n_beads - 1)

    fil = ['tail'] + ['motor'] * (n_beads - 3) + ['core', 'head']

    top = simulation.add_topology(
        "filament", fil + fil, init_pos
    )

    links = np.full((len(init_pos), 3), -1, dtype=int)
    links[:n_beads - 1, 1] = np.arange(1, n_beads)
    links[n_beads: 2 * n_beads - 1, 1] = n_beads + np.arange(1, n_beads)
    links[1: n_beads, 0] = np.arange(n_beads - 1)
    links[n_beads + 1: 2 * n_beads, 0] = n_beads + np.arange(n_beads - 1)

    graph = top.get_graph()
    for j in range(n_beads - 1):
        graph.add_edge(j, j + 1)
        graph.add_edge(j + n_beads, j + 1 + n_beads)
    for j in range(1, n_beads-2):
        graph.add_edge(j, j + n_beads)
        links[j, 2] = j+n_beads
        links[j+n_beads, 2] = j

    filaments = [Filament(0, 0, links), Filament(1, n_beads, links)]
    filament_handler.initialize(filaments, links)

    return system, simulation
