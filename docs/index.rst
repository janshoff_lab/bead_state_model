.. bead_state_model documentation master file, created by
   sphinx-quickstart on Fri Jan 10 12:42:54 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

``bead_state_model``
====================

``bead_state_model`` is a python package to simulate biopolymers like actin and microtubules.
Polymers are modeled as linearly connected beads. The polymers can form connections
via cross-links or motors. Instead of explicitly simulating these binding
proteins, they are implicitly represented by the state of a bead in a polymer,
hence the name ``bead_state_model``.

.. image:: _static/species_isolated_and_reactions.svg

This package was developed with systems that include *external* particles in mind.
``bead_state_model`` is a wrapper around the simulation framework `ReaDDy2 <https://readdy.github.io/>`_,
and leverages ReaDDy's python API to implement polymers, cross-links and motors. To
add non-polymer particles, the user can make full use of ReaDDy's functionality to define
particles and bonds and reactions between particles. This allows users to build systems like
these:

.. figure:: _static/systems_microrheo_and_indentation.svg

   (Left) Slice of a cross-linked 3D network with a passive microsphere for microrheology measurements.
   (Right) A microsphere gets pressed into a layer of polymers.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered: 3

   install
   simulations/index
   network_assembly
   examples/index
   file_formats
   code/index
   reaction_scenarios

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
