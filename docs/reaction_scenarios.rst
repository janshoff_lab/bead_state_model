
.. include:: roles.rst
	  
Reaction Scenarios
******************
	  
Motor Binding
=============

* Integration Test Coverage: :good:`good`.
* Performance Test Coverage: :critical:`None!`

* Added distance option to topology-topology reactions, now implementation in ReaDDy is straight forward.
* Distance (number of edges between binding candidates) needs to be set :math:`d > 1`, s.th. direct neighbors within same filament can't form a motor bond.

Cases
-----

#. Two filaments binding (merging into one topology in ReaDDy).
#. Two filaments belonging to same topolgy can bind (if distance parameter :math:`d` allows it).

Motor Step
==========

* Integration Test Coverage: :poor:`poor`.
* Performance Test Coverage: :critical:`None!`

Cases
-----

#. Normal step towards end of filament.
#. Multiple motors on filament, walking as long as forward neighbor is :state-core:`core`.
#. :strike:`Multiple motors can perform a step at the same time step.`
   :strike:`Might have to restrict to one candidate per motor-motor pair, at least for now.`
   (postponing handling multiple reactions per time step)
#. Rate has to decrease with larger distance! Two motors can pull on the same filament
   in opposite directions.

Motor Unbinding
===============

* Integration Test Coverage: :critical:`None!`
* Performance Test Coverage: :critical:`None!`

* Start with equal unbinding rate for all motors. Unbinding
  rate increasing with energy might be necessary at some point.

Cross-link Binding/Unbinding
============================

* Integration Test Coverage: :critical:`None!`
* Performance Test Coverage: :critical:`None!`

* Same as Motor binding/unbinding.

Filament Polymerization
=======================

* Integration Test Coverage: :good:`good`.
* Performance Test Coverage: :critical:`None!`

* Implemented Polymerization / attachment of monomers as a structural topology reaction.
  A monomer spawns from background concentration and is attached
  to a filament's :state-head:`head`. The old :state-head:`head` turns into a :state-core:`core`,
  and the newly spawned bead becomes the :state-head:`head` of the filament.

  
Cases
-----

#. Filament can grow.
#. :strike:`Multiple filaments can grow at the same time step.` (not possible
   for filaments within the same topology for now)
  
Filament Depolymerization
=========================

* Integration Test Coverage: :good:`good`.
* Performance Test Coverage: :critical:`None!`

* Each filament has a chance to detach the :state-tail:`tail`, the neighbouring :state-core:`core`
  becomes new :state-tail:`tail`. For one filament, this can only happen once per time step.

Multiple Reactions in Parallel
==============================

**Update:** I asked the question on `github <https://github.com/readdy/readdy/issues/175>`_.
This is by design, only one reaction per topology per time step. But the developers gave
directions where this could be changed in the code.

With current reactions (2020-01-13), growing polymers and motor steps
apparently never occur at the same point in time for the same topology.
This is concerning, since it means some reactions might be favored over others.
If we have a global mega-topology like a cross-linked network, it could happen
that it only performs steps, but not attach or detach monomers anymore.
:critical:`This needs to be investigated!`
