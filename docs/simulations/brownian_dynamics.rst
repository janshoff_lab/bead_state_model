
Brownian Dynamics
=================

``bead_state_model`` is a wrapper for the general reaction-diffusion simulation
framework `ReaDDy <https://readdy.github.io/>`_. ``bead_state_model`` handles
the configuration of biopolymers using ``ReaDDy``\ 's python interface.
The actual simulations are performed by ``ReaDDy``. It uses the overdamped
Langevin equation for Brownian dynamics simulations. Integration of this
equation of motion is performed by an Euler integrator. Details can be found
in their `manual <https://readdy.github.io/simulation.html#simulation_run>`_
as well as in
`this publication <https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006830>`_.

Reactions
=========

Reactions are per default handled by a
`Gillespie reaction handler <https://readdy.github.io/simulation.html#reaction-handler>`_
in ``ReaDDy``. Cross-link and motor binding and unbinding, filament polymerization and
depolymerization, as well as motor steps are
handled by this reaction handler.

Units
=====

``bead_state_model`` uses the `unitless configuration <https://readdy.github.io/system.html>`_ of ``ReaDDy``.
This means that

* :math:`k_B T = 1`,
* lengths are in units of some fundamental length scale :math:`x_0`,
* similarly, times are in units of a time scale :math:`t_0`.

Beads are set up to have a diameter of :math:`x_0` and a diffusion coefficient of :math:`D = 1`.
The length of :math:`x_0` in
physical units has to be tuned, for example to match a realistic persistence length
as described in the example :ref:`tune-l-p`.

Once you defined :math:`x_0`, one natural choice of time scale :math:`t_0` would be
to choose it such that the diffusion coefficient of :math:`D=1` in internal units
matches that of a bead of radius :math:`x_0/2` in a medium of a certain viscosity
(viscosity of water or cytosol, etc.) and temperature that matches your system.
