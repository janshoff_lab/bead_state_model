.. _filamenthandler:

FilamentHandler
===============

All filaments and networks of filaments (connected with cross-link or motor bonds)
are topologies in `ReaDDy <https://readdy.github.io/system.html#topologies>`_.
To implement the reactions of biopolymers
(polymerization, depolymerization, motor binding, motor steps) I defined
reactions on these topologies. Some reactions require the information
in which direction along the filament is the head, in which direction is the
tail (for motor steps), or the information how many motors are present
in the current topology (to determine the rate for reactions), or similar
information of the graph of a topology. Figuring these information
out whenever such a reaction happens, would slow down the simulations.
To circumvent this, I implemented the class
:class:`bead_state_model.filament_handler.FilamentHandler`, which acts as
an elaborate table to keep track of:

* Which topology contains which filaments (filaments are assigned IDs).
* Which vertices of a topology are motors.
* What are the ordered items (beads) of each filament (beads are assigned IDs).

With these information, methods of ``FilamentHandler`` are defined
that are used as rate and reaction callback functions for the various reactions.
