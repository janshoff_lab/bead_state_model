Install
*******

``bead_state_model`` can be installed in a conda environment,
which was only tested on GNU/Linux. However, there is also the option
to run the ``bead_state_model`` Docker image (see below), which should also work on windows or mac.

Install in conda Environment
============================

ReaDDy2 is designed to be install with `conda <https://docs.conda.io/en/latest/>`_, hence I recommend
using conda for `bead_state_model` as well.

Prepare a new conda environment
-------------------------------

Install `miniconda3 <https://docs.conda.io/en/latest/miniconda.html>`_.

Add the conda-forge channel for ReaDDy2 and create a new environment:

.. code:: bash

   # add conda-forge channel
   conda config --add channels conda-forge

   # create environment
   conda create -n bead_state

   # activate new environment
   conda activate bead_state

   # Install readdy, usually that would be:
   # conda install -c readdy readdy
   # But bead_state_model uses some features of the
   # not-yet-released development version, so instead
   # use:
   conda install -c readdy/label/dev readdy

Verify installation of ``readdy`` by starting python and importing it,

.. code::

   python
   # ...
   >>> import readdy

Install ``bead_state_model``
----------------------------

Install with ``pip`` and ``git`` from project repository:

.. code::

   pip install git+https://gitlab.gwdg.de/janshoff_lab/bead_state_model

Alternatively, if you don't have ``git`` installed, go to the
`project repository website <https://gitlab.gwdg.de/janshoff_lab/bead_state_model>`_,
use the download button (next to the clone button) to download the source code in
an archive, e.g. zip. Use pip to install the file you downloaded:

.. code::

   pip install path/to/downloaded/file.zip

Install ``bead_state_model`` Docker Container
=============================================

``bead_state_model`` is available as a docker image. To run the image,
you have to install Docker as described in `Docker Docs <https://docs.docker.com/get-started/>`_.
Next, run the ``bead_state_model`` container, which will trigger the download of the
image when done for the first time:

.. code:: bash

   docker run -d --name bsm ilyask/bead_state_model

This will spawn the container, and to access it via terminal, you have to use
``docker exec``:

.. code:: bash

   docker exec -it bsm bash

Which will put you at a bash terminal inside the container at location ``/miniconda``.
You can try that it worked out, by starting a python session and importing ``bead_state_model``:

.. code:: bash

   python
   # ...
   >>> import bead_state_model

While this would theoretically put you in a position to start using ``bead_state_model``,
it's more convenient to simulation or analysis scripts first, and mount the folder
containing the scripts to the docker container. Let's assume your scripts (and any
config files that you might use inside the scripts) are inside a folder named ``scripts``,
which is a subfolder of your current working directory. You would have to start the docker
container like so:

.. code::

   docker run -d --name bsm --volume "$(pwd)/scripts:/bsm" --workdir /bsm ilyask/bead_state_model

Note that you might have to stop (``docker kill bsm``) and remove (``docker rm bsm``) the
``bsm`` container first, if you have executed the first command of this section before.
One way to circumvent that is to assign a new name to the container with the ``scripts``
folder mounted, you could for example use ``bsm_scripts`` as the argument to the ``--name``
option.

Once you started the container, you should now be able to access it via bash again
(``docker exec -it bsm bash``). The ``--workdir /bsm`` option above set the working
directory to ``/bsm``, which is the location where the contents of the ``scripts``
folder were mounted. Hence, when you list the contents of the current folder in the
bash session, you should see the contents of your ``scripts`` folder there.

Now you should be able to run simulations inside the container. For example, assuming
your simulation script is named ``run_sim.py``, and resides inside the ``scripts`` folder,
in the bash session connected to the container, execute:

.. code:: bash

   python run_sim.py


