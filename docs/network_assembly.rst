
.. _NetworkAssembly:

Network Assembly
================

The package ``bead_state_model.network_assembly`` contains tools to generate
polymers and polymer networks. This section explains the network generation
algorithm of the class
:class:`~bead_state_model.network_assembly.create_network_simple.CreateNetworkSimple`,
which is what you probably want to use for most networks.
The suffix "simple" in the class' name is to discriminate it from
:class:`~bead_state_model.network_assembly.create_network_natural.CreateNetworkNatural`,
which was implemented first. The class
:class:`~bead_state_model.network_assembly.create_network_natural.CreateNetworkNatural`
generates networks with polymers of different lengths (following a
`Poisson distribution <https://en.wikipedia.org/wiki/Poisson_distribution>`_).
This would reflect more naturalistic networks, but makes analyses more complicated.
Use it only, if you expect some effect due to polymers of different lengths.

To start network generation, call method
:meth:`~bead_state_model.network_assembly.create_network_simple.CreateNetworkSimple.run`
of class
:class:`~bead_state_model.network_assembly.create_network_simple.CreateNetworkSimple`.


Nucleation of Polymers
----------------------

These steps are performed for placement of the first two beads of :math:`N` polymers,
referred to as nucleation of the polymers:

.. admonition:: Algorithm - Nucleation of Polymers

   For :math:`N` polymers, do the following:

   1. Place the first bead of the polymer at a random position :math:`r` in the simulation box.
   2. If the position overlaps with a previoulsy placed bead, go back to step 1.
   3. For the second bead, draw distance :math:`l` from a normal distribution with
      mean :math:`\mu_l = 1 x_0` and standard deviation :math:`\sigma_l = 1/\sqrt{k_\text{stretch}}`.
   4. Draw a random direction :math:`(\phi, \theta) \in [0, 2\pi) \times [0, \pi)`.
   5. Check if placing second bead on designated position :math:`(\phi, \theta)` on sphere with radius :math:`l`
      around the first bead would lead to overlap.
   6. If it leads to collision, go back to step 3.
   7. Place bead at designated position.

At the end of this part, there are :math:`2N` beads in the simulation box. Currently, overlap is not checked for 
between consecutive beads of the same filament. In contrast, the function
:func:`~bead_state_model.network_assembly.create_filament.create_filament` to create single filaments
does not allow consecutive beads to overlap (this was implemented, because filaments were supposed to work
also with simulation software that considered beads as hard spheres). In the actual simulations, it's somewhere in between.
A repulsive harmonic interaction potential makes overlap energetically unfavorable, even between consecutive beads.
Either way, during simulation the length distributions equlibrate rather quickly. 
See section `Equilibration of Polymers`_ how
length distribution (distance between beads) changes during simulation.

The direction from first to second bead is drawn randomly on default. This can be changed by
swapping the default value of the attribute
:attr:`~bead_state_model.network_assembly.create_network.CreateNetwork.nucleation_direction_func`
with a custom one.

Elongation
----------

After nucleation, polymers are being elongated by adding more beads. Beads are placed following the direction
of last segment plus some noise.

.. admonition:: Algorithm - Elongation of Polymers

   Repeat the following :math:`3 \le i \le M` times:

   For each of the :math:`N` filaments append one bead this way:

   1. Determine the unit direction vector :math:`u` pointing from second to last to last bead.
   2. Draw random distance :math:`l` from a normal distribution with
      mean :math:`\mu_l = 1 x_0` and standard deviation :math:`\sigma_l = 1/\sqrt{k_\text{stretch}}`.
   3. Draw random angle :math:`\theta` from normal distribution with mean
      :math:`\mu_\theta = 0` and standard deviation :math:`\sigma_\theta = 1/\sqrt{k_\text{bend}}`.
   4. Draw random rotation from uniform distribution :math:`[0, \pi)`.
   5. Construct vector :math:`v = (l, 0, 0)`.
   6. Rotate vector around anchor :math:`(0, 0, 1)` by angle :math:`\theta`,
      yielding :math:`v_\theta = (l\cos\theta, l\sin\theta, 0)`.
   7. Rotate vector around anchor :math:`(1, 0, 0)` by angle :math:`\gamma`,
      yielding :math:`v_{\theta, \gamma} = (l\cos\theta, l\sin\theta\cos\gamma, l\sin\theta\sin\gamma)`.
   8. Construct rotation matrix :math:`R` that would rotate vector :math:`(1, 0, 0)` onto :math:`u`,
      use it to rotate :math:`v_{\theta, \gamma}`.
   9. Add the final vector to the position of the last bead. This is the designated position
      of the bead to append.
   10. Check for overlap at designated position. If it would lead to overlap, repeat from step 1.
   11. Place bead at designated position.

At the end of the process, :math:`N` polymers with math:`M` beads each will be in the simulation box.
Class :class:`~bead_state_model.network_assembly.create_network_simple.CreateNetworkSimple` restricts
the number of retries for placement. If after 10000 retries (default) no free space for the bead to
be placed was found, it is skipped. This means you could end up with less than the attempted
:math:`N\cdot M` beads in crowded simulation boxes. You might want to filter out too short polymers
before you start the simulations.

