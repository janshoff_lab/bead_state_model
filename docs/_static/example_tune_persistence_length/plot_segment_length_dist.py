from typing import Iterable, List
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from bead_state_model.data_reader import DataReader
from bead_state_model.periodic_boundaries import get_minimum_image_vector


def get_segment_lengths(indices: Iterable[int], frames: Iterable[int],
                        folder_name_template: str, parts=2) -> List[np.ndarray]:
    """
    For all indices, read data in respective folder, and compute length of 
    segments (distance between consecutive coordinates) at given frames. This function 
    assumes that data in folders is data of a single filament, with beads
    of the filament in sequential order.
    """
    folders = [
        folder_name_template.format(idx) for idx in indices
    ]
    data_all = [[] for i in range(len(frames))]
    for idx, f in zip(indices, folders):
        dr = DataReader(f+'/part_{:04}/data.h5', n_parts=parts)
        box = dr.read_box_size()
        box = np.array([box[1, 0] - box[0, 0],
                        box[1, 1] - box[0, 1],
                        box[1, 2] - box[0, 2]])
        positions = dr.read_particle_positions()
        for i, t in enumerate(frames):
            pos_t = positions[t] + box / 2
            segment_vectors = get_minimum_image_vector(pos_t[1:], pos_t[:-1], box)
            segment_lengths = np.sqrt((segment_vectors ** 2).sum(1))
            data_all[i].append(segment_lengths)
    return [np.concatenate(di) for di in data_all]


if __name__ == "__main__":

    n_bins = 60
    # plt.rcParams.update({"text.usetex": True})
    
    index = pd.read_csv('index.csv', index_col=0)

    results_dir = os.path.join('results', 'segment_length')
    os.makedirs(results_dir, exist_ok=True)

    folder_sim_template = os.path.join('simulations', 'out{:04}')

    # frames at which to evaluate segment length distributions
    frames = [0, 2000, 4000]

    # prepare dataframe for results
    multi_index = pd.MultiIndex.from_product((frames, index['k_bend'].unique()),
                                             names=['frame', 'k_bend'])
    dist_results = pd.DataFrame(index=multi_index,
                                columns=['mean', 'var'])

    for k_bend in index['k_bend'].unique():
        print('at k_bend =', k_bend)
        # prepare figure for plots at different frames
        fig, ax = plt.subplots(len(frames), 1, sharex=True, sharey=True,
                               figsize=(7, 8))

        selected = index[index['k_bend'] == k_bend]

        # list of arrays of segment length, one array for each requested frame
        segment_lengths = get_segment_lengths(np.array(selected.index),
                                              frames, folder_sim_template)

        for j, t in enumerate(frames):
            sl = segment_lengths[j]
            values, edges, _ = ax[j].hist(sl, n_bins, alpha=0.3, density=True)
            ax[j].step(edges[:-1], values, where='post', color='black', lw=0.5)
            ax[j].vlines([edges[0], edges[-1]], 0, [values[0], values[-1]], lw=0.5)
            ax[j].set_title('distribution at frame {}'.format(t))
            mean = sl.mean()
            var = sl.var()

            ax[j].vlines([mean], 0, values.max()+0.1, linewidth=3.0, color='C1', label='mean')
            ax[j].vlines([mean-np.sqrt(var), mean+np.sqrt(var)], 0, values.max()+0.1,
                         linewidth=3.0, color='C2', label='+/- std')
            
            dist_results.loc[t, k_bend] = mean, var

        ax[0].legend()
        ax[-1].set_xlabel('segment length $\ell$')
        fig.suptitle('k_bend = {:.1f}'.format(k_bend))
        fig.tight_layout()
        fig.subplots_adjust(top=0.889)
        fig.savefig(os.path.join(results_dir,
                                 f'histograms_k_bend_{k_bend:.1f}.svg'))

    dist_results.to_csv(os.path.join(results_dir, 'mean_var.csv'))

    plt.show()
