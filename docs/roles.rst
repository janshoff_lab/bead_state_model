.. role:: good

.. role:: check

.. role:: critical

.. role:: poor

.. role:: state-core

.. role:: state-motor

.. role:: state-clink

.. role:: state-head

.. role:: state-tail

.. role:: state-substrate

.. role:: strike
