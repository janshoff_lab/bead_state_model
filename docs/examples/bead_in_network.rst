
.. _example bead in network:

Bead in Network
===============

Perform simulations of passive microrheology experiments. A bead is placed
in a dense actin network.

For statistical significance, it is always a good idea to repeat simulations several times.
In this example, we create 6 networks randomly with identical parameters. We vary one parameter in
4 steps: the binding rate of motors. For each combination of initial network and motor binding rate,
we start 2 repetitions, yielding a total number of 48 simulations.

Example Files
-------------

The files can be found zipped `here <../_static/example_microrheo.zip>`_. Download and unzip it.
This should leave you with a folder with 4 files:

.. code:: shell

   example_microrheo
   ├── create_networks.py
   ├── default_params.json
   ├── run_equilibrations.py
   └── run_simulations.py


Create Networks
---------------

The first script you have to run is ``create_networks.py``. Run it e.g. with:

.. code::

   python create_networks.py 3

The number in the end is the number of processes to run in parallel. Adjust it
to match ne number of processes that you want to use (more than 6 won't have any benefit,
as only 6 networks will be generated).
The script will generate 6 networks with 17500 particles in total
(forming 700 filaments with 25 beads each).

The script saves the generated networks in folders ``initial_states/network000`` to
``initial_states/network005``. An index table is generated and saved as
``initial_states/index.csv``.

Equilibrate
-----------

The network generation algorithm creates isolated filaments nearly equilibrated, but that's not
necessarily the case for crowded networks. And when it comes to placing large beads like the
one we want to use for microrheology, the algorithm is not too successful either. To remove
potential overlaps of filaments and the larger bead, we need to carefully equilibrate the system.

We do that here by running Brownian dynamics simulations of the 6 networks with very small time steps
for several thousand steps with the script ``run_equilibrations.py``. These simulations might
take 30 minutes up to several hours each, depending
on the computing power of your CPU. I recommend you run as many of the processes as you can in parallel.
It's 6 networks that we want to equilibrate, so 6 processes would be ideal. Pass the
number of processes to be run in parallel to the script when you execute it in the
command line.

The results of the simulations are stored in folder ``initial_states_equilibrated``.

Note the essential difference when setting up simulations with external particles, which lies in the
arguments passed to the ``Simulation`` class:

.. code::
   
   s = Simulation(
       output_folder=output_folder,
       parameters=params,
       non_filament_particles={'bead': bead_diffusion_const},
       interaction_setup_function=mr_bead_setup_func
   )

The ``non_filament_particles`` argument expects a dictionary mapping
particle names (strings) to diffusion constants (floats). The
``interaction_setup_function`` needs to be an function that takes an instance
of ReaDDy2's ``ReactionDiffusionSystem``, and uses its methods to
define interactions of the external particles. In our example that is:

.. code::
   
   def mr_bead_setup_func(system: readdy.ReactionDiffusionSystem):
       radius = mr_bead_parameters['radius']
       k_repulsion = mr_bead_parameters['k_repulsion']
       for other in ['core', 'head', 'tail', 'motor']:
           system.potentials.add_harmonic_repulsion(
               "bead", other,
               force_constant=k_repulsion,
               interaction_distance=.5 + radius
           )

The function iterates over all filament particles and defines harmonic repulsion between them and the
MR bead (``"bead"``).


Simulations
-----------

Now to the actual simulations. The simulations are run for all combinations of parameters defined
in the variable ``varied_parameters`` in the script:

.. code::

   varied_parameters = {
       'rate_motor_bind': [0.0, 0.1, 0.25, 0.5],
       'initial_state_index': np.arange(6)
   }

The function call to ``_create_parameter_table`` creates these combinations twice
(due to multiplier 2) and stores them in a
pandas DataFrame:

.. code::

   parameter_table = _create_parameter_table(varied_parameters, 2, offset)

In the DataFrame, each of these :math:`4 \cdot 6 \cdot 2 = 48` parameter sets is assigned an
unique index (sequential integer),
that we then use to defer the output folder name.

The number of steps to be simulated (and related to that: the number of frames / data points to
capture of the simulation) is defined in the file ``default_params.json``. For this example,
the number is set very low, to have the simulations finish within reasonable time. You can increase
the number of steps, if you want to run proper scientific simulations (simulations of 100.000 steps
take around 5-20 days on our compute cluster, ideally I would run all 48 simulations in parallel).

If you don't adjust them the default parameters regarding the number of simulated steps are

.. code:: javascript

   "n_steps": 100,
   "observation_interval": 5

``n_steps`` is the total number of simulated steps. Every 5 steps, one frame will be added to the resulting output file.

Once you are happy with the current parameters, execute the script. This will create
the folder ``simulations`` with 48 subfolders ``out0000`` to ``out0047``. In addition,
an index table will be written to ``simulations/simulation_index.csv``. This script as well
expects you to pass the number of parallel processes as an argument.
   
Reading Data
------------

Visualize
.........

To visualize the particle trajectories, ``readdy`` provides some means to export
them to ``xyz`` or ``tcl`` (I don't use ``tcl``, so I set it to ``False`` here):

.. literalinclude:: ../../examples/passive_microrheology/analysis/export_xyz.py

Microrheology Bead Trajectories
...............................

.. literalinclude:: ../../examples/passive_microrheology/analysis/plot_microrheo_bead_trajectory.py

