

.. |mu| unicode:: U+03BC

.. _tune-l-p:
		  
Tune Persistence Length
=======================

In this example we run lots of repetitions of single filament simulations to tune the bending
modulus ``k_bend`` s.th. it leads to a desired persistence length :math:`l_p`.

You can find the files for this example in `this archive <../_static/example_tune_persistence_length.zip>`_.
It should contain a folder with 3 files:

.. code::

   example_tune_persistence_length
   ├── default_params.json
   ├── run_create_filament.py
   └── run_simulations.py


Choose Segmentation
-------------------

First of all, to tune the length scale, you have to define the segmentation of your
filaments and the length of your filaments. For example, in our lab we often work
with actin filaments of average contour length :math:`l_c = 15`\ |mu|\ m.
In this example, I want 15\ |mu|\ m of filament to be segmented into 25 beads or 24 segments.

The script ``run_create_filament.py`` creates filaments consisting of 25 beads. It's set up
to create those filaments with varying bending modulus ``k_bend``, which we will
need for tuning the persistence length :math:`l_p` later on. For now, we are only interested
in the average distance between beads, :math:`\langle l \rangle`. The bead diameter is set
to 1, it defines our internal length scale :math:`x_0`. The harmonic potential between two beads also has
it's resting point at a distance of 1. However, the average segment length will be a bit larger than 1,
due to the repulsion between beads. It will depend on the stretching modulus
``k_stretch`` and on the repulsion between beads ``k_repulsion``, but not much on
the bending modulus, at least as long as you are still within a semi-flexible regime (more or less
straight filaments) rather than a freely rotating chain.

The average distance between beads :math:`\langle l \rangle` will give us the average contour
length of a filament. We defined
that 24 segments should equal 15\ |mu|\ m, so this will allow us to compute :math:`x_0`:

.. math::

   \langle l \rangle \cdot 24 &= \frac{15 \text{µm}}{x_0}\\
   \Leftrightarrow x_0 &= \frac{15 \text{µm}}{\langle l \rangle \cdot 24}

We need to run a first set of simulations to determine :math:`\langle l \rangle` for our values
of ``k_stretch`` and ``k_repulsion``. You can adjust these values in ``default_params.json``, if
you like, but the default settings should be fine. As mentioned above, the values of
``k_bend`` should not have much influence, so you can leave them set to ``[13, 26]``
in the script ``run_create_filament.py`` that will create 60 filaments with ``k_bend=13``
and ``k_bend=26`` each. Run the script now. This will generate the folder ``initial_states``
and 120 subfolders ``filament0000`` to ``filament0119``. In addition, it will
create ``index.csv`` that stores indices and bending moduli.

After that, run the script ``run_simulations.py``, but
**note that this starts the simulations, which will probably take several hours!** It
will run simulations for all 120 filaments, in the default setup it will do so for
:math:`2\cdot 10^6` simulation steps. You might consider reducing the number of steps
in ``default_params.json`` if you only want to play around with the simulations for now.

Running the simulations produces the folder ``simulations`` and subfolders
``out0000`` to ``out0119``.

.. _visualization:

Visualization
.............

It's always a good idea to look at a 3D visualization of a few simulations to inspect
whether the filaments show expected behaviour. The results of simulations
are split into parts, in the default setting of ``n_steps``, ``observation_interval``
and ``next_part_after_n_frames`` (in ``default_params.json``), data are split
into two parts for each simulation.

You can use ``readdy``\ 's export function to export both parts manually:

.. sourcecode::
   :caption: export_xyz.py

   import readdy

   for p in range(2):
       traj = readdy.Trajectory('simulations/out0000/part_{:04}/data.h5'.format(p))
       traj.convert_to_xyz(generate_tcl=False)

I use `ovito <https://www.ovito.org/>`_ for visualization of ``xyz`` files, hence I set
``generate_tcl=False``. Leave that option out if you need ``tcl`` files.

.. figure:: ../_static/example_tune_persistence_length/animation.gif

   First 50 frames (looped) of ``simulations/out0000/part_0000/data.h5``, visualized
   with ovito.

If you want to have parts stitched together to one ``xyz`` file, I recommend you use the
``actomyosin_analyser`` package (see repository `here <https://gitlab.gwdg.de/ikuhlem/actomyosin_analyser>`_):

.. sourcecode::
   :caption: export_xyz.py

   from bead_state_model.data_reader import DataReader
   from actomyosin_analyser.analysis.analyser import Analyser
   from actomyosin_analyser.file_io.xyz_exporter import XYZExporter
   
   dr = DataReader('simulations/out0000/part_{:04}/data.h5', n_parts=2)
   a = Analyser(dr, 'simulations/out0000/analysis.h5')
   
   expo = XYZExporter()
   
   expo.export_all(a, folder='simulations/out0000/xyz')

This will take longer (pure python implementation) and create larger files, because it also
contains information of orientation to draw the filaments
with cylinders rather than spheres:

.. figure:: ../_static/example_tune_persistence_length/animation_cylinder.gif

   First 50 frames (looped) of ``simulations/out0000/part_0000/data.h5`` and
   ``simulations/out0000/part_0000/data.h5``, visualized
   with ovito. ``xyz`` file exported with ``actomyosin_analyser``.

When filaments are looking similar to the examples shown here, continue with analysis of the segment lengths.

Segment Length
..............

Analyse the segment lengths, e.g. with
`this script <../_static/example_tune_persistence_length/plot_segment_length_dist.py>`_.

It writes means and variances at different points in time to file ``results/segment_length/mean_var.csv``.
In addition, it produces plots like these:

.. image:: ../_static/example_tune_persistence_length/seglength_dist.svg

The segment length distribution at frame 0 shows, that the filament generation algorithm will strictly
create non-overlapping beads, hence the hard cut at 1.0, which equals the bead diameter. This
will relax quickly, and after a few hundred simulated steps, the distrubtion should not change much anymore.
These plots include data of only 20 repetitions. With 60 repetitions as in the default settings of this
example, there should be less variation in the means. But even in our smaller subset, there
is little variation in the means (apart from frame 0) across points in time and across different
bending moduli. Verify that this is true for your simulations as well by looking at the
produced ``csv`` file:

.. csv-table:: mean_var.csv
   :file: ../_static/example_tune_persistence_length/mean_var.csv
   :header-rows: 1

For :math:`\langle l \rangle`, I will take the average of the four mean values at non-zero frames:

.. math::

   \langle l \rangle &\approx 1.154 \\
   \Rightarrow x_0 &= \frac{15\;\text{µm}}{1.154 \cdot 24}\\
   &= 0.5416\;\text{µm}

This defines our conversion from internal units to physical untits and vice versa.

Persistence Length
------------------

Let's try to tune ``k_bend`` such that we get a persistence length of :math:`17\;\text{µm}`
(as for example in Broedersz2014Modeling_), or in internal units:

.. math::

   \ell_p = \frac{17\;\text{µm}}{x_0} = 31.34

The persistence length is defined via the exponential decay of tangent-tangent correlations
along the filament (Broedersz2014Modeling_, Rubinstein2003Polymer_):

.. math::

   \langle \vec{t}(s) \cdot \vec{t}(s') \rangle = \exp(-|s-s'|/\ell_p)

In our discrete model, we use the cosines of angles between two segments :math:`i, j`
where :math:`i < j`,

.. math::

   t_i t_j = \cos\theta_{ij},

and the distance between them along the contour,

.. math::

   d_{ij} = \sum_{k=i}^{j-1} ||r_{k+1} - r_{k}||.

We do this for all :math:`(i, j)` pairs, for all our simulations, and take a binned average
of our cosine-distance pairs :math:`(d_{ij}, \cos\theta_{ij})`, to evaluate the
exponential decays along our filaments (which I have done with
`this script <../_static/example_tune_persistence_length/plot_segment_length_dist.py>`_):

.. image:: ../_static/example_tune_persistence_length/cos_theta/cos_theta.svg

To estimate :math:`\ell_p`, we fit decaying exponentials to these curves. Note, that we
have only few data points towards the tail of the curves (larger distances along the filaments),
and that you should not include the full curves for the fit. This might need some manual tuning.

.. image:: ../_static/example_tune_persistence_length/cos_theta/cos_theta_fit.svg

The script puts these plots and a table with fit results into folder ``results/cos_theta``.

.. csv-table:: Fit Results
   :file: ../_static/example_tune_persistence_length/cos_theta/fit_results.csv
   :header-rows: 1

You can see that ``k_bend = 26`` leads to too large persistence length values, our aim was
:math:`\ell_p \approx 31.34`. ``k_bend = 13`` leads to too small a value. With only 20 repetitions
this is not totally reliable, if you execute the ``run_create_filament.py`` and ``run_simulations.py``
script in the default settings, you should end up with 60 repetitions. Thus, if you didn't change
anything, your results should be more reliable. You can now adjust those scripts to run simulations
with different ``k_bend`` values, to scan the parameter range further and iteratively try to find
a value that matches your desired persistence length.
	   
Literature
----------

.. _Broedersz2014Modeling:

*  Broedersz, C. P., & MacKintosh, F. C. (2014). Modeling semiflexible polymer networks. Reviews of Modern Physics, 86(3), 995. (`link <https://journals.aps.org/rmp/abstract/10.1103/RevModPhys.86.995>`_)
   
.. _Rubinstein2003Polymer:

* Rubinstein, M., & Colby, R. H. (2003). Polymer physics (Vol. 23). New York: Oxford university press.
