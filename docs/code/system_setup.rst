
``system_setup``
****************

For users, the most important function are in module :py:mod:`bead_state_model.system_setup`.

.. automodule:: bead_state_model.system_setup
    :members:
