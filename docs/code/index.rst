Source Code Documentation
=========================

.. toctree::
   :maxdepth: 1

   system_setup
   components
   filament_handler
   data_reader
   network_assembly
