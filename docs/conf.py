# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('..'))


# -- Project information -----------------------------------------------------

project = 'bead_state_model'
copyright = '2020, Ilyas Kuhlemann'
author = 'Ilyas Kuhlemann'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
import sphinx_rtd_theme
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- custom stuff -------------------------------------------------------------

import os
import tempfile
import shutil

# create archive from example files

def _create_archive_from_example_files(
        temp_dir,
        example_dir_name,
        files
):
    dname = os.path.join(temp_dir.name, example_dir_name)
    os.mkdir(dname)
    for f in files:
        shutil.copy(f, dname)
    shutil.make_archive(f'_static/{example_dir_name}', 'zip', temp_dir.name, example_dir_name)


d = tempfile.TemporaryDirectory()
_create_archive_from_example_files(
    d,
    'example_microrheo',
    [f'../examples/passive_microrheology/{f}' for f in ['create_networks.py',
                                                        'default_params.json',
                                                        'run_equilibrations.py',
                                                        'run_simulations.py']]
)
# d = tempfile.TemporaryDirectory()
_create_archive_from_example_files(
    d,
    'example_tune_persistence_length',
    [f'../examples/tune_persistence_length/{f}' for f in ['default_params.json',
                                                          'run_create_filament.py',
                                                          'run_simulations.py']]
)
_create_archive_from_example_files(
    d,
    'example_basic_network',
    [f'../examples/simple_example/{f}' for f in ['create_network.py',
                                                 'run_simulation.py']]
)

shutil.copy('../examples/tune_persistence_length/plot_segment_length_dist.py',
            '_static/example_tune_persistence_length')
shutil.copy('../examples/tune_persistence_length/plot_cos_theta.py',
            '_static/example_tune_persistence_length')
